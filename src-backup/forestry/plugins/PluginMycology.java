//package forestry.plugins;
//
//import java.util.Random;
//
//import net.minecraft.block.Block;
//import net.minecraft.command.ICommand;
//import net.minecraft.item.Item;
//import net.minecraft.item.ItemStack;
//import net.minecraft.src.ModLoader;
//import net.minecraft.world.World;
//import binnie.botany.api.FlowerManager;
//import binnie.botany.api.IFlowerMutation;
//import binnie.botany.core.BlockBotany;
//import binnie.botany.core.BlockLoam;
//import binnie.botany.core.BotanyCore;
//import binnie.botany.core.FlowerBreedingSystem;
//import binnie.botany.core.ItemBotany;
//import binnie.botany.core.ItemLoam;
//import binnie.botany.core.ItemRendererBotany;
//import binnie.botany.core.TileEntityPlant;
//import binnie.botany.genetics.AlleleFlowerStem;
//import binnie.botany.genetics.FlowerBreedingManager;
//import binnie.botany.genetics.FlowerHelper;
//import binnie.botany.genetics.FlowerMutation;
//import binnie.botany.genetics.FlowerSpecies;
//import binnie.botany.items.ItemDictionary;
//import binnie.botany.items.ItemTrowel;
//import binnie.core.BinnieCore;
//import cpw.mods.fml.common.network.IGuiHandler;
//import forestry.api.core.IOreDictionaryHandler;
//import forestry.api.core.IPacketHandler;
//import forestry.api.core.IPickupHandler;
//import forestry.api.core.IPlugin;
//import forestry.api.core.IResupplyHandler;
//import forestry.api.core.ISaveEventHandler;
//import forestry.api.core.ItemInterface;
//import forestry.api.genetics.AlleleManager;
//import forestry.api.genetics.IAllele;
//
////import binnie.botany.genetics.FlowerBranch;
//
//public class PluginMycology implements IPlugin {
//
//	public static Block blockMushroom;
//	public static Item itemMushroom;
//	public static Item itemTrowel;
//
//	@Override
//	public boolean isAvailable() {
//		return true;
//	}
//
//	@Override
//	public void preInit() {
//	}
//
//	@Override
//	public void doInit() {
//
//		blockMushroom = new BlockBotany(2290);
//		itemMushroom = new ItemBotany(2292);
//		itemTrowel = new ItemTrowel(2293);
//		new ItemDictionary(2294);
//
//		ModLoader.registerBlock(blockMushroom);
//	}
//
//	
//
//	@Override
//	public void postInit() {
//	
//		ModLoader.addRecipe(
//				new ItemStack(itemTrowel),
//				new Object[] { " BR", " SB", "S  ", Character.valueOf('B'),
//						ItemInterface.getItem("ingotBronze"),
//						Character.valueOf('R'),
//						new ItemStack(Item.dyePowder, 1, 1),
//						Character.valueOf('S'), Item.stick });
//	}
//
//	@Override
//	public String getDescription() {
//		return "Mushroom and Fungus breeding";
//	}
//
//	@Override
//	public void generateSurface(World world, Random rand, int chunkX, int chunkZ) {
//	}
//
//	@Override
//	public IGuiHandler getGuiHandler() {
//		return null;
//	}
//
//	@Override
//	public IPacketHandler getPacketHandler() {
//		return null;
//	}
//
//	@Override
//	public IPickupHandler getPickupHandler() {
//		return null;
//	}
//
//	@Override
//	public IResupplyHandler getResupplyHandler() {
//		return null;
//	}
//
//	@Override
//	public ISaveEventHandler getSaveEventHandler() {
//		return null;
//	}
//
//	@Override
//	public IOreDictionaryHandler getDictionaryHandler() {
//		return null;
//	}
//
//	@Override
//	public ICommand[] getConsoleCommands() {
//		return null;
//	}
//
//}
