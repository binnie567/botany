package forestry.plugins;

import java.util.Random;

import net.minecraft.block.Block;
import net.minecraft.command.ICommand;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.src.ModLoader;
import net.minecraft.world.World;
import binnie.botany.api.FlowerManager;
import binnie.botany.api.IFlowerMutation;
import binnie.botany.core.BlockBotany;
import binnie.botany.core.BlockLoam;
import binnie.botany.core.BotanyCore;
import binnie.botany.core.FlowerBreedingSystem;
import binnie.botany.core.ItemBotany;
import binnie.botany.core.ItemLoam;
import binnie.botany.core.ItemRendererBotany;
import binnie.botany.core.TileEntityPlant;
import binnie.botany.farm.CircuitGarden;
import binnie.botany.genetics.AlleleFlowerStem;
import binnie.botany.genetics.FlowerBreedingManager;
import binnie.botany.genetics.FlowerHelper;
import binnie.botany.genetics.FlowerMutation;
import binnie.botany.genetics.FlowerSpecies;
import binnie.botany.items.ItemDictionary;
import binnie.botany.items.ItemTrowel;
import binnie.core.BinnieCore;
import cpw.mods.fml.common.network.IGuiHandler;
import forestry.api.circuits.ChipsetManager;
import forestry.api.circuits.ICircuitLayout;
import forestry.api.core.IOreDictionaryHandler;
import forestry.api.core.IPacketHandler;
import forestry.api.core.IPickupHandler;
import forestry.api.core.IPlugin;
import forestry.api.core.IResupplyHandler;
import forestry.api.core.ISaveEventHandler;
import forestry.api.core.ItemInterface;
import forestry.api.genetics.AlleleManager;
import forestry.api.genetics.IAllele;
import forestry.core.circuits.Circuit;
import forestry.core.circuits.ItemSolderingIron.SolderingInventory;
import forestry.core.config.ForestryItem;

//import binnie.botany.genetics.FlowerBranch;

public class PluginBotany implements IPlugin {

	public static Block blockBotany;
	public static Block blockLoam;
	public static Item itemBotany;

	@Override
	public boolean isAvailable() {
		return true;
	}

	@Override
	public void preInit() {
		FlowerManager.flowerInterface = new FlowerHelper();
		BotanyCore.flowerBreedingSystem = new FlowerBreedingSystem();
	}

	@Override
	public void doInit() {

		FlowerManager.breedingManager = new FlowerBreedingManager();

		for (IAllele allele : AlleleFlowerStem.values()) {
			AlleleManager.alleleRegistry.registerAllele(allele);
		}

		for (FlowerSpecies species : FlowerSpecies.values()) {
			AlleleManager.alleleRegistry.registerAllele(species);
			FlowerManager.breedingManager.registerFlowerTemplate(
					species.getUID(), species.getTemplate());
		}

		for (IFlowerMutation mutation : FlowerMutation.values()) {
			FlowerManager.flowerMutations.add(mutation);
		}

		// Block bb = new BlockBotany(2291);
		// ModLoader.registerBlock(bb);
		blockBotany = new BlockBotany(2290);
		blockLoam = new BlockLoam(2291);
		itemBotany = new ItemBotany(2292);
		itemTrowel = new ItemTrowel(2293);
		new ItemDictionary(2294);

		ModLoader.registerBlock(blockLoam, ItemLoam.class);

		ModLoader.registerBlock(blockBotany);

		/*
		 * if (BinnieCore.proxy.isClient()) { List<ItemStack> loamBlocks = new
		 * ArrayList<ItemStack>(); blockLoam.getSubBlocks(0, null, loamBlocks);
		 * for (ItemStack loam : loamBlocks) ModLoader .addName(loam,
		 * BlockLoam.getName(loam.getItemDamage())); }
		 */

		BinnieCore.proxy
				.registerTileEntity(
						TileEntityPlant.class,
						"botany.plant",
						BinnieCore.proxy
								.createObject("binnie.botany.core.RendererPlant"));

		BinnieCore.proxy.registerCustomItemRenderer(itemBotany.itemID,
				new ItemRendererBotany());

	}

	Item itemTrowel;

	@Override
	public void postInit() {
		// FlowerBranch.init();
		for (IFlowerMutation mutation : FlowerMutation.values()) {
			FlowerManager.flowerMutations.add(mutation);
		}

		ModLoader.addRecipe(
				new ItemStack(itemTrowel),
				new Object[] { " BR", " SB", "S  ", Character.valueOf('B'),
						ItemInterface.getItem("ingotBronze"),
						Character.valueOf('R'),
						new ItemStack(Item.dyePowder, 1, 1),
						Character.valueOf('S'), Item.stick });
		
		CircuitGarden circuitGarden = new CircuitGarden();
		
		ChipsetManager.circuitRegistry.registerCircuit(circuitGarden);
		
		ICircuitLayout layoutManaged = ChipsetManager.circuitRegistry.getLayout("forestry.farms.managed");
		
		ChipsetManager.solderManager.addRecipe(layoutManaged, new ItemStack(Item.appleRed), circuitGarden);
		
		
	}

	@Override
	public String getDescription() {
		return "Flower and Herb breeding";
	}

	@Override
	public void generateSurface(World world, Random rand, int chunkX, int chunkZ) {
	}

	@Override
	public IGuiHandler getGuiHandler() {
		return null;
	}

	@Override
	public IPacketHandler getPacketHandler() {
		return null;
	}

	@Override
	public IPickupHandler getPickupHandler() {
		return null;
	}

	@Override
	public IResupplyHandler getResupplyHandler() {
		return null;
	}

	@Override
	public ISaveEventHandler getSaveEventHandler() {
		return null;
	}

	@Override
	public IOreDictionaryHandler getDictionaryHandler() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ICommand[] getConsoleCommands() {
		// TODO Auto-generated method stub
		return null;
	}

}
