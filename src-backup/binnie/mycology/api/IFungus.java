package binnie.mycology.api;

import net.minecraft.world.World;
import forestry.api.genetics.IIndividual;

public interface IFungus extends IIndividual {

	IFungusGenome getFlowerGenome();

	public void age();

	public IFungus die(World world);

	public void mate(IFungusGenome genome);

	int getAge();

	public boolean isAlive();

	public boolean isPollinated();

	public IFungus createOffspring(World world);

	int getDamage();

	void setAge(int age);

}
