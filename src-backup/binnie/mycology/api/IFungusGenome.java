package binnie.mycology.api;

import forestry.api.genetics.IGenome;

public interface IFungusGenome extends IGenome {

	public IAlleleFungusSpecies getPrimaryAsFlower();

}
