package binnie.mycology.api;

import forestry.api.genetics.IAllele;
import forestry.api.genetics.IGenome;
import forestry.api.genetics.IMutation;

public interface IFungusMutation extends IMutation {

	int getChance(IAllele allele0, IAllele allele1, IGenome genome0,
			IGenome genome1);

}
