package binnie.mycology.api;

import net.minecraft.item.ItemStack;
import forestry.api.genetics.IAllele;
import forestry.api.genetics.IChromosome;

public interface IFungusInterface {

	boolean isFungus(ItemStack stack);

	IFungus getFungus(ItemStack stack);

	IFungus getFungus(IFungusGenome genome, int age);

	ItemStack getItemStack(IFungus flower);

	IChromosome[] templateAsChromosomes(IAllele[] template);

	IChromosome[] templateAsChromosomes(IAllele[] templateActive,
			IAllele[] templateInactive);

	IFungusGenome templateAsGenome(IAllele[] template);

	IFungusGenome templateAsGenome(IAllele[] templateActive,
			IAllele[] templateInactive);

}
