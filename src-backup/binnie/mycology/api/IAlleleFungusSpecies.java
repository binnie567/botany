package binnie.mycology.api;

import forestry.api.genetics.IAlleleSpecies;

public interface IAlleleFungusSpecies extends IAlleleSpecies {

	public String getTextureFile();

	public int getPrimaryIconIndex();

	public int getSecondaryIconIndex();
}
