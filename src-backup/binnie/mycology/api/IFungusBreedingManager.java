package binnie.mycology.api;

import net.minecraft.world.World;
import forestry.api.genetics.IAllele;

public interface IFungusBreedingManager {

	int getSpeciesCount();

	void registerTemplate(IAllele[] template);

	void registerTemplate(String identifier, IAllele[] template);

	IAllele[] getTemplate(String identifier);

	IAllele[] getDefaultTemplate();

	IMycologistTracker getMycologistTracker(World world);

}
