package binnie.mycology.api;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.nbt.NBTTagCompound;
import forestry.api.genetics.IAlleleSpecies;
import forestry.api.genetics.IIndividual;
import forestry.api.genetics.IMutation;

public interface IMycologistTracker {

	void registerFungus(EntityPlayer player, IIndividual flower);

	int getFungusCount();

	int getSpeciesBred();

	void registerMutation(IMutation mutation);

	boolean isDiscovered(IMutation mutation);

	boolean isDiscovered(IAlleleSpecies species);

}
