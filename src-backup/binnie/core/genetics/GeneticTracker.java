package binnie.core.genetics;

import java.util.ArrayList;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.world.WorldSavedData;
import forestry.api.genetics.IAllele;
import forestry.api.genetics.IAlleleSpecies;
import forestry.api.genetics.IIndividual;
import forestry.api.genetics.IMutation;

public class GeneticTracker extends WorldSavedData implements IGeneticTracker {

	public GeneticTracker(String par1Str) {
		super(par1Str);
	}

	@Override
	public void decodeFromNBT(NBTTagCompound nbttagcompound) {
		readFromNBT(nbttagcompound);
	}

	@Override
	public void encodeToNBT(NBTTagCompound nbttagcompound) {
		writeToNBT(nbttagcompound);
	}

	@Override
	public void registerIndividual(EntityPlayer player, IIndividual individual) {
		individualCount++;
		IAlleleSpecies primary = individual.getGenome().getPrimary();
		IAlleleSpecies secondary = individual.getGenome().getSecondary();

		registerSpecies(primary);
		registerSpecies(secondary);
		markDirty();
	}

	@Override
	public int getIndividualCount() {
		return 0;
	}

	@Override
	public void registerMutation(IMutation mutation) {
		discoveredMutations.add(mutation.getAllele0().getUID() + "-"
				+ mutation.getAllele1().getUID());
		markDirty();
	}

	private void registerSpecies(IAllele species) {
		if (!discoveredSpecies.contains(species.getUID()))
			discoveredSpecies.add(species.getUID());
	}

	@Override
	public boolean isDiscovered(IMutation mutation) {
		return discoveredMutations.contains(mutation.getAllele0().getUID()
				+ "-" + mutation.getAllele1().getUID());
	}

	@Override
	public boolean isDiscovered(IAlleleSpecies species) {
		return discoveredSpecies.contains(species.getUID());
	}
	
	@Override
	public void readFromNBT(NBTTagCompound nbttagcompound) {
		individualCount = nbttagcompound.getInteger("IndividualCount");

		// / SPECIES
		discoveredSpecies = new ArrayList<String>(256);
		int count = nbttagcompound.getInteger("SpeciesCount");
		for (int i = 0; i < count; i++)
			discoveredSpecies.add(nbttagcompound.getString("SD" + i));

		// / MUTATIONS
		discoveredMutations = new ArrayList<String>();
		count = nbttagcompound.getInteger("MutationsCount");
		for (int i = 0; i < count; i++)
			discoveredMutations.add(nbttagcompound.getString("MD" + i));

	}
	
	private int individualCount;

	@Override
	public void writeToNBT(NBTTagCompound nbttagcompound) {

		nbttagcompound.setInteger("IndividualCount", individualCount);

		// / SPECIES
		nbttagcompound.setInteger("SpeciesCount", discoveredSpecies.size());
		for (int i = 0; i < discoveredSpecies.size(); i++)
			if (discoveredSpecies.get(i) != null)
				nbttagcompound.setString("SD" + i, discoveredSpecies.get(i));

		// / MUTATIONS
		nbttagcompound.setInteger("MutationsCount", discoveredMutations.size());
		for (int i = 0; i < discoveredMutations.size(); i++)
			if (discoveredMutations.get(i) != null)
				nbttagcompound.setString("MD" + i, discoveredMutations.get(i));

	}

	private ArrayList<String> discoveredSpecies = new ArrayList<String>(256);
	private ArrayList<String> discoveredMutations = new ArrayList<String>();
	
	@Override
	public int getSpeciesCount() {
		return discoveredSpecies.size();
	}

	@Override
	public int getMutationCount() {
		return discoveredMutations.size();
	}

	@Override
	public void synchToPlayer(EntityPlayer player) {
	}
	
}
