package binnie.core.genetics;

import java.util.Collection;

import net.minecraft.world.World;
import forestry.api.genetics.IAllele;
import forestry.api.genetics.IMutation;

public interface IGeneticBreedingManager {

	int getSpeciesCount();

	void registerTemplate(IAllele[] template);

	void registerTemplate(String identifier, IAllele[] template);

	IAllele[] getTemplate(String identifier);

	IAllele[] getDefaultTemplate();

	IGeneticTracker getTracker(World world, String player);

	void registerMutation(IMutation mutation);

	Collection<IMutation> getMutations();

}
