package binnie.core.genetics;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.nbt.NBTTagCompound;
import forestry.api.genetics.IAlleleSpecies;
import forestry.api.genetics.IIndividual;
import forestry.api.genetics.IMutation;

public interface IGeneticTracker {
	
	void decodeFromNBT(NBTTagCompound nbttagcompound);

	void encodeToNBT(NBTTagCompound nbttagcompound);

	void registerIndividual(EntityPlayer player, IIndividual individual);

	int getIndividualCount();
	
	int getSpeciesCount();
	
	int getMutationCount();

	void registerMutation(IMutation mutation);

	boolean isDiscovered(IMutation mutation);

	boolean isDiscovered(IAlleleSpecies species);

	void synchToPlayer(EntityPlayer player);

}
