package binnie.core.genetics;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;

import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.world.World;
import binnie.botany.api.FlowerManager;
import binnie.botany.api.IAlleleFlowerSpecies;
import binnie.botany.api.IFlower;
import binnie.botany.api.IFlowerGenome;
import binnie.botany.api.IFlowerInterface;
import binnie.botany.genetics.BotanistTracker;
import binnie.botany.genetics.Flower;
import forestry.api.genetics.AlleleManager;
import forestry.api.genetics.IAllele;
import forestry.api.genetics.IAlleleSpecies;
import forestry.api.genetics.IChromosome;
import forestry.api.genetics.IGenome;
import forestry.api.genetics.IIndividual;
import forestry.api.genetics.IMutation;
import forestry.core.genetics.Chromosome;
import forestry.plugins.PluginBotany;

public abstract class GeneticBreedingManager implements IGeneticBreedingManager {

	GeneticBreedingManager(BreedingSystem system) {
		this.breedingSystem = system;
	}
	
	BreedingSystem breedingSystem;
	
	public static int speciesCount = -1;
	
	public BreedingSystem getBreedingSystem() {
		return breedingSystem;
	}
	
	@Override
	public int getSpeciesCount() {
		if (speciesCount < 0) {
			speciesCount = 0;
			Iterator it = AlleleManager.alleleRegistry.getRegisteredAlleles()
					.entrySet().iterator();
			while (it.hasNext()) {
				Entry<String, IAllele> entry = (Entry<String, IAllele>) it
						.next();
				if (getBreedingSystem().isSpecies(entry.getValue()))
					if (((IAlleleSpecies) entry.getValue()).isCounted())
						speciesCount++;
			}
		}

		return speciesCount;
	}
	
	public static HashMap<String, IAllele[]> speciesTemplates = new HashMap<String, IAllele[]>();

	@Override
	public void registerTemplate(IAllele[] template) {
		registerTemplate(template[0].getUID(), template);
	}

	@Override
	public void registerTemplate(String identifier, IAllele[] template) {
		speciesTemplates.put(identifier, template);
	}

	@Override
	public IAllele[] getTemplate(String identifier) {
		return speciesTemplates.get(identifier);
	}

	@Override
	public IAllele[] getDefaultTemplate() {
		return null;//getBreedingSystem().getInterface().getDefaultTemplate();
	}
	
	String getPrefix() {
		return "Genetic";
	}
	
	Class<? extends GeneticTracker> getTrackerClass() {
		return GeneticTracker.class;
	}

	@Override
	public IGeneticTracker getTracker(World world, String player) {
		GeneticTracker tracker = (GeneticTracker) world.loadItemData(
				getTrackerClass(), getPrefix() + "Tracker");

		// Create a tracker if there is none yet.
		if (tracker == null) {
			try {
				tracker = getTrackerClass().getConstructor(getTrackerClass()).newInstance((getPrefix() + "Tracker"));
				world.setItemData(getPrefix() + "Tracker", tracker);
			} catch (Exception  e) {
			}
			
		}

		return tracker;
	}
	
	List<IMutation> mutations = new ArrayList<IMutation>();

	@Override
	public void registerMutation(IMutation mutation) {
		mutations.add(mutation);
	}

	@Override
	public Collection<IMutation> getMutations() {
		return mutations;
	}
}
