package binnie.core.genetics;

import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.world.World;
import binnie.botany.api.IFlower;
import binnie.botany.api.IFlowerGenome;
import binnie.botany.api.IFlowerInterface;
import forestry.api.genetics.IAllele;
import forestry.api.genetics.IChromosome;
import forestry.api.genetics.IGenome;
import forestry.api.genetics.IIndividual;
import forestry.core.genetics.Chromosome;
import forestry.plugins.PluginBotany;

public abstract class GeneticHelper<IndividualClass, GenomeClass> implements IGeneticInterface<IndividualClass, GenomeClass> {

	@Override
	public abstract boolean isIndividual(ItemStack stack);

	@Override
	public abstract IndividualClass getIndividual(ItemStack stack);

	@Override
	public abstract IndividualClass getIndividual(World world, IGenome genome);

	@Override
	public abstract IndividualClass getIndividual(World world, IGenome genome,
			IIndividual mate);

	@Override
	public abstract ItemStack getItemStack(IIndividual bee);

	@Override
	public abstract GenomeClass templateAsGenome(IAllele[] template);

	@Override
	public abstract GenomeClass templateAsGenome(IAllele[] templateActive,
			IAllele[] templateInactive);
	
	

	
	/*
	
	@Override
	public abstract boolean isIndividual(ItemStack stack);

	@Override
	public abstract IIndividual getIndividual(ItemStack stack);

	@Override
	public abstract IIndividual getIndividual(IGenome genome) {
		return new Flower(genome, age);
	}

	@Override
	public ItemStack getFlowerStack(IFlower flower) {

		NBTTagCompound nbttagcompound = new NBTTagCompound();
		flower.writeToNBT(nbttagcompound);
		ItemStack beeStack = new ItemStack(PluginBotany.itemBotany, 1,
				flower.getDamage());
		beeStack.setTagCompound(nbttagcompound);
		return beeStack;
	}

	@Override
	public Chromosome[] templateAsChromosomes(IAllele[] template) {
		Chromosome[] chromosomes = new Chromosome[template.length];
		for (int i = 0; i < template.length; i++)
			if (template[i] != null)
				chromosomes[i] = new Chromosome(template[i]);

		return chromosomes;
	}

	@Override
	public Chromosome[] templateAsChromosomes(IAllele[] templateActive,
			IAllele[] templateInactive) {
		Chromosome[] chromosomes = new Chromosome[templateActive.length];
		for (int i = 0; i < templateActive.length; i++)
			if (templateActive[i] != null)
				chromosomes[i] = new Chromosome(templateActive[i],
						templateInactive[i]);

		return chromosomes;
	}

	@Override
	public IFlowerGenome templateAsGenome(IAllele[] template) {
		return new FlowerGenome(templateAsChromosomes(template));
	}

	@Override
	public IFlowerGenome templateAsGenome(IAllele[] templateActive,
			IAllele[] templateInactive) {
		return new FlowerGenome(templateAsChromosomes(templateActive,
				templateInactive));
	}

	@Override
	public IAllele[] getDefaultFlowerTemplate() {
		IAllele[] template = new IAllele[2];
		template[0] = FlowerSpecies.Rose;
		template[1] = AlleleFlowerStem.Thorn;
		return template;

	}

	@Override
	public IFlowerGenome getDefaultFlowerGenome() {
		return templateAsGenome(getDefaultFlowerTemplate());
	}
	*/

}
