package binnie.core.genetics;

import net.minecraft.item.ItemStack;
import net.minecraft.world.World;
import forestry.api.apiculture.EnumBeeType;
import forestry.api.apiculture.IBee;
import forestry.api.apiculture.IBeeGenome;
import forestry.api.genetics.IAllele;
import forestry.api.genetics.IChromosome;
import forestry.api.genetics.IGenome;
import forestry.api.genetics.IIndividual;

public interface IGeneticInterface<IndividualClass, GenomeClass> {
	
	boolean isIndividual(ItemStack stack);

	IndividualClass getIndividual(ItemStack stack);

	IndividualClass getIndividual(World world, IGenome genome);

	IndividualClass getIndividual(World world, IGenome genome, IIndividual mate);

	ItemStack getItemStack(IIndividual bee);
	
	IAllele[] getDefaultTemplate();

	//IChromosome[] templateAsChromosomes(IAllele[] template);

	//IChromosome[] templateAsChromosomes(IAllele[] templateActive, IAllele[] templateInactive);

	GenomeClass templateAsGenome(IAllele[] template);

	GenomeClass templateAsGenome(IAllele[] templateActive, IAllele[] templateInactive);

}
