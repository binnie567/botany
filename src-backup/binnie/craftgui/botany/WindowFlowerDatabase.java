package binnie.craftgui.botany;

import net.minecraft.entity.player.EntityPlayer;
import binnie.botany.core.BotanyCore;
import binnie.botany.core.BotanyTexture;
import binnie.craftgui.mod.database.PageBranchSpecies;
import binnie.craftgui.mod.database.WindowAbstractDatabase;
import binnie.craftgui.window.Window;
import cpw.mods.fml.relauncher.Side;

public class WindowFlowerDatabase extends WindowAbstractDatabase {

	public WindowFlowerDatabase(EntityPlayer player, Side side, boolean nei) {
		super(player, side, nei, BotanyCore.flowerBreedingSystem, 140f);
	}

	public static Window create(EntityPlayer player, Side side, boolean nei) {
		return new WindowFlowerDatabase(player, side, nei);
	}

	@Override
	protected void addTabs() {
		speciesPages.addPage(new PageSpeciesOverview(speciesPages), new Tab(
				"Overview", 0));

		branchPages.addPage(new PageBranchOverview(branchPages), new Tab(
				"Overview", 0));
		branchPages.addPage(new PageBranchSpecies(branchPages), new Tab(
				"Species", 0));
	}
	
	@Override
	public String getBackgroundTextureFile() {
		return BotanyTexture.guiPath + "ItemRenderer";
	}

}
