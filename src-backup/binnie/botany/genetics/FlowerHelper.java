package binnie.botany.genetics;

import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import binnie.botany.api.IFlower;
import binnie.botany.api.IFlowerGenome;
import binnie.botany.api.IFlowerInterface;
import forestry.api.genetics.IAllele;
import forestry.core.genetics.Chromosome;
import forestry.plugins.PluginBotany;

public class FlowerHelper implements IFlowerInterface {

	@Override
	public boolean isFlower(ItemStack stack) {
		if (stack == null)
			return false;

		return stack.itemID == PluginBotany.itemBotany.itemID;
	}

	@Override
	public IFlower getFlower(ItemStack stack) {
		if (!isFlower(stack))
			return null;
		return new Flower(stack.getTagCompound());
	}

	@Override
	public IFlower getFlower(IFlowerGenome genome, int age) {
		return new Flower(genome, age);
	}

	@Override
	public ItemStack getFlowerStack(IFlower flower) {

		NBTTagCompound nbttagcompound = new NBTTagCompound();
		flower.writeToNBT(nbttagcompound);
		ItemStack beeStack = new ItemStack(PluginBotany.itemBotany, 1,
				flower.getDamage());
		beeStack.setTagCompound(nbttagcompound);
		return beeStack;
	}

	@Override
	public Chromosome[] templateAsChromosomes(IAllele[] template) {
		Chromosome[] chromosomes = new Chromosome[template.length];
		for (int i = 0; i < template.length; i++)
			if (template[i] != null)
				chromosomes[i] = new Chromosome(template[i]);

		return chromosomes;
	}

	@Override
	public Chromosome[] templateAsChromosomes(IAllele[] templateActive,
			IAllele[] templateInactive) {
		Chromosome[] chromosomes = new Chromosome[templateActive.length];
		for (int i = 0; i < templateActive.length; i++)
			if (templateActive[i] != null)
				chromosomes[i] = new Chromosome(templateActive[i],
						templateInactive[i]);

		return chromosomes;
	}

	@Override
	public IFlowerGenome templateAsGenome(IAllele[] template) {
		return new FlowerGenome(templateAsChromosomes(template));
	}

	@Override
	public IFlowerGenome templateAsGenome(IAllele[] templateActive,
			IAllele[] templateInactive) {
		return new FlowerGenome(templateAsChromosomes(templateActive,
				templateInactive));
	}

	@Override
	public IAllele[] getDefaultFlowerTemplate() {
		IAllele[] template = new IAllele[2];
		template[0] = FlowerSpecies.Rose;
		template[1] = AlleleFlowerStem.Thorn;
		return template;

	}

	@Override
	public IFlowerGenome getDefaultFlowerGenome() {
		return templateAsGenome(getDefaultFlowerTemplate());
	}

}
