package binnie.botany.genetics;

import binnie.botany.api.FlowerManager;
import binnie.botany.api.IAlleleFlowerSpecies;
import binnie.botany.api.IFlowerMutation;
import forestry.api.genetics.IAllele;
import forestry.api.genetics.IGenome;

public enum FlowerMutation implements IFlowerMutation {

	Test(FlowerSpecies.Dandelion, FlowerSpecies.Rose,
			FlowerSpecies.AfricanMarigold),

	;

	IAllele allele0;
	IAllele allele1;
	IAllele[] template;

	private FlowerMutation(IAllele allele0, IAllele allele1, IAllele[] template) {
		this.allele0 = allele0;
		this.allele1 = allele1;
		this.template = template;
	}

	private FlowerMutation(IAllele allele0, IAllele allele1,
			IAlleleFlowerSpecies species) {
		this(allele0, allele1, FlowerManager.breedingManager
				.getFlowerTemplate(species.getUID()));
	}

	@Override
	public IAllele getAllele0() {
		return allele0;
	}

	@Override
	public IAllele getAllele1() {
		return allele1;
	}

	@Override
	public IAllele[] getTemplate() {
		return template;
	}

	@Override
	public int getBaseChance() {
		return 100;
	}

	@Override
	public boolean isPartner(IAllele allele) {
		return allele.getUID().equals(allele0.getUID())
				|| allele.getUID().equals(allele1.getUID());
	}

	@Override
	public IAllele getPartner(IAllele allele) {
		return allele.getUID().equals(allele0.getUID()) ? allele1 : allele0;
	}

	@Override
	public boolean isSecret() {
		return false;
	}

	@Override
	public int getChance(IAllele allele0, IAllele allele1, IGenome genome0,
			IGenome genome1) {

		if (!getPartner(allele0).getUID().equals(allele1.getUID()))
			return 0;

		return getBaseChance();
	}

}
