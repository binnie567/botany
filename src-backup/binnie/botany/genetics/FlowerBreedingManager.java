package binnie.botany.genetics;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;

import net.minecraft.world.World;
import binnie.botany.api.FlowerManager;
import binnie.botany.api.IAlleleFlowerSpecies;
import binnie.botany.api.IBotanistTracker;
import binnie.botany.api.IFlower;
import binnie.botany.api.IFlowerBreedingManager;
import forestry.api.genetics.AlleleManager;
import forestry.api.genetics.IAllele;

public class FlowerBreedingManager implements IFlowerBreedingManager {

	public static int flowerSpeciesCount = -1;

	@Override
	public int getFlowerSpeciesCount() {
		if (flowerSpeciesCount < 0) {
			flowerSpeciesCount = 0;
			Iterator it = AlleleManager.alleleRegistry.getRegisteredAlleles()
					.entrySet().iterator();
			while (it.hasNext()) {
				Entry<String, IAllele> entry = (Entry<String, IAllele>) it
						.next();
				if (entry.getValue() instanceof IAlleleFlowerSpecies)
					if (((IAlleleFlowerSpecies) entry.getValue()).isCounted())
						flowerSpeciesCount++;
			}
		}

		return flowerSpeciesCount;
	}

	public static HashMap<String, IAllele[]> speciesTemplates = new HashMap<String, IAllele[]>();
	public static ArrayList<IFlower> flowerTemplates = new ArrayList<IFlower>();

	@Override
	public void registerFlowerTemplate(IAllele[] template) {
		registerFlowerTemplate(template[0].getUID(), template);
	}

	@Override
	public void registerFlowerTemplate(String identifier, IAllele[] template) {
		flowerTemplates.add(new Flower(FlowerManager.flowerInterface
				.templateAsGenome(template), 3));
		speciesTemplates.put(identifier, template);
	}

	@Override
	public IAllele[] getFlowerTemplate(String identifier) {
		return speciesTemplates.get(identifier);
	}

	@Override
	public IAllele[] getDefaultFlowerTemplate() {
		return FlowerManager.flowerInterface.getDefaultFlowerTemplate();
	}

	@Override
	public IBotanistTracker getBotanistTracker(World world) {
		BotanistTracker tracker = (BotanistTracker) world.loadItemData(
				BotanistTracker.class, "BotanistTracker");

		// Create a tracker if there is none yet.
		if (tracker == null) {
			tracker = new BotanistTracker("BotanistTracker");
			world.setItemData("BotanistTracker", tracker);
		}

		return tracker;
	}

}
