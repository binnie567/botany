package binnie.botany.genetics;

import java.util.ArrayList;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.world.WorldSavedData;
import binnie.botany.api.IBotanistTracker;
import forestry.api.genetics.IAllele;
import forestry.api.genetics.IAlleleSpecies;
import forestry.api.genetics.IIndividual;
import forestry.api.genetics.IMutation;
import forestry.core.config.Defaults;

public class BotanistTracker extends WorldSavedData implements IBotanistTracker {

	public BotanistTracker(String par1Str) {
		super(par1Str);
	}

	@Override
	public void decodeFromNBT(NBTTagCompound nbttagcompound) {
		readFromNBT(nbttagcompound);
	}

	@Override
	public void encodeToNBT(NBTTagCompound nbttagcompound) {
		writeToNBT(nbttagcompound);
	}

	@Override
	public void registerFlower(EntityPlayer player, IIndividual flower) {
		flowersTotal++;
		IAlleleSpecies primary = flower.getGenome().getPrimary();
		IAlleleSpecies secondary = flower.getGenome().getSecondary();

		registerSpecies(primary);
		registerSpecies(secondary);
		markDirty();
	}

	@Override
	public int getFlowerCount() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void registerMutation(IMutation mutation) {
		discoveredMutations.add(mutation.getAllele0().getUID() + "-"
				+ mutation.getAllele1().getUID());
		markDirty();
	}

	private void registerSpecies(IAllele species) {
		if (!discoveredSpecies.contains(species.getUID()))
			discoveredSpecies.add(species.getUID());
	}

	@Override
	public boolean isDiscovered(IMutation mutation) {
		return discoveredMutations.contains(mutation.getAllele0().getUID()
				+ "-" + mutation.getAllele1().getUID());
	}

	@Override
	public boolean isDiscovered(IAlleleSpecies species) {
		return discoveredSpecies.contains(species.getUID());
	}

	@Override
	public int getSpeciesBred() {
		return discoveredSpecies.size();
	}

	@Override
	public void readFromNBT(NBTTagCompound nbttagcompound) {
		flowersTotal = nbttagcompound.getInteger("FlowersTotal");

		// / SPECIES
		discoveredSpecies = new ArrayList<String>(Defaults.SPECIES_BEE_LIMIT);
		int count = nbttagcompound.getInteger("SpeciesCount");
		for (int i = 0; i < count; i++)
			discoveredSpecies.add(nbttagcompound.getString("SD" + i));

		// / MUTATIONS
		discoveredMutations = new ArrayList<String>();
		count = nbttagcompound.getInteger("MutationsCount");
		for (int i = 0; i < count; i++)
			discoveredMutations.add(nbttagcompound.getString("MD" + i));

	}

	@Override
	public void writeToNBT(NBTTagCompound nbttagcompound) {

		nbttagcompound.setInteger("FlowersTotal", flowersTotal);

		// / SPECIES
		nbttagcompound.setInteger("SpeciesCount", discoveredSpecies.size());
		for (int i = 0; i < discoveredSpecies.size(); i++)
			if (discoveredSpecies.get(i) != null)
				nbttagcompound.setString("SD" + i, discoveredSpecies.get(i));

		// / MUTATIONS
		nbttagcompound.setInteger("MutationsCount", discoveredMutations.size());
		for (int i = 0; i < discoveredMutations.size(); i++)
			if (discoveredMutations.get(i) != null)
				nbttagcompound.setString("MD" + i, discoveredMutations.get(i));

	}

	private ArrayList<String> discoveredSpecies = new ArrayList<String>(256);
	private ArrayList<String> discoveredMutations = new ArrayList<String>();

	private int flowersTotal = 0;

}
