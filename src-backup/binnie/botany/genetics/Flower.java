package binnie.botany.genetics;

import java.util.Collections;
import java.util.List;
import java.util.Random;

import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.world.World;
import binnie.botany.api.FlowerManager;
import binnie.botany.api.IAlleleFlowerSpecies;
import binnie.botany.api.IAlleleFlowerStem;
import binnie.botany.api.IBotanistTracker;
import binnie.botany.api.IFlower;
import binnie.botany.api.IFlowerGenome;
import binnie.botany.api.IFlowerMutation;
import binnie.botany.core.BotanyCore;
import forestry.api.genetics.IAllele;
import forestry.api.genetics.IChromosome;
import forestry.api.genetics.IGenome;
import forestry.api.genetics.IIndividual;
import forestry.core.genetics.Chromosome;
import forestry.core.genetics.Individual;

public class Flower extends Individual implements IFlower {

	public IFlowerGenome genome;
	public IFlowerGenome mate;

	public Flower(NBTTagCompound nbttagcompound) {
		readFromNBT(nbttagcompound);
	}

	public Flower(IFlowerGenome genome, int age) {

		this.genome = genome;

		this.age = age;
	}

	@Override
	public String getDisplayName() {
		IAlleleFlowerSpecies species = getFlowerGenome().getPrimaryAsFlower();
		IAlleleFlowerStem stem = getFlowerGenome().getStem();

		String name = "";

		if (species != null)
			name += species.getName();

		if (age == 0)
			name += " Seed";

		return name;
	}

	@Override
	public void addTooltip(List<String> list) {
	}

	@Override
	public IGenome getGenome() {
		return genome;
	}

	@Override
	public String getIdent() {
		return null;
	}

	@Override
	public void readFromNBT(NBTTagCompound nbttagcompound) {

		super.readFromNBT(nbttagcompound);

		if (nbttagcompound == null) {
			this.genome = BotanyCore.flowerInterface.getDefaultFlowerGenome();
			return;
		}
		age = nbttagcompound.getInteger("Age");

		if (nbttagcompound.hasKey("Genome")) {
			genome = new FlowerGenome(nbttagcompound.getCompoundTag("Genome"));
		}

		if (nbttagcompound.hasKey("Mate"))
			mate = new FlowerGenome(nbttagcompound.getCompoundTag("Mate"));

	}

	@Override
	public void writeToNBT(NBTTagCompound nbttagcompound) {

		super.writeToNBT(nbttagcompound);

		nbttagcompound.setInteger("Age", age);

		if (genome != null) {
			NBTTagCompound NBTmachine = new NBTTagCompound();
			genome.writeToNBT(NBTmachine);
			nbttagcompound.setTag("Genome", NBTmachine);
		}
		if (mate != null) {
			NBTTagCompound NBTmachine = new NBTTagCompound();
			mate.writeToNBT(NBTmachine);
			nbttagcompound.setTag("Mate", NBTmachine);
		}

	}

	@Override
	public int getDamage() {

		return getAge() + getFlowerGenome().getStem().getIconIndex() * 1024
				+ getFlowerGenome().getPrimaryAsFlower().getPrimaryIconIndex()
				* 16;
	}

	@Override
	public IFlowerGenome getFlowerGenome() {
		return (IFlowerGenome) getGenome();
	}

	int age = 1;

	final int MAX_AGE = 6;

	@Override
	public void age() {
		age += 1;
	}

	@Override
	public int getAge() {
		return age;
	}

	@Override
	public boolean isAlive() {
		return age < MAX_AGE;
	}

	@Override
	public IFlower createOffspring(World world) {

		if (mate == null)
			return null;

		IChromosome[] chromosomes = new IChromosome[genome.getChromosomes().length];
		IChromosome[] parent1 = genome.getChromosomes();
		IChromosome[] parent2 = mate.getChromosomes();

		IChromosome[] mutated1 = mutateSpecies(world, genome, mate);
		if (mutated1 != null)
			parent1 = mutated1;
		IChromosome[] mutated2 = mutateSpecies(world, mate, genome);
		if (mutated2 != null)
			parent2 = mutated2;

		for (int i = 0; i < parent1.length; i++)
			if (parent1[i] != null && parent2[i] != null)
				chromosomes[i] = inheritChromosome(world.rand, parent1[i],
						parent2[i]);

		return new Flower(new FlowerGenome(chromosomes), 0);
	}

	private IChromosome[] mutateSpecies(World world, IFlowerGenome genome,
			IFlowerGenome mate) {

		IChromosome[] parent1 = genome.getChromosomes();
		IChromosome[] parent2 = mate.getChromosomes();

		IGenome genome0;
		IGenome genome1;
		IAllele allele0;
		IAllele allele1;

		if (world.rand.nextBoolean()) {
			allele0 = parent1[EnumFlowerChromosome.SPECIES.ordinal()]
					.getPrimaryAllele();
			allele1 = parent2[EnumFlowerChromosome.SPECIES.ordinal()]
					.getSecondaryAllele();

			genome0 = genome;
			genome1 = mate;
		} else {
			allele0 = parent2[EnumFlowerChromosome.SPECIES.ordinal()]
					.getPrimaryAllele();
			allele1 = parent1[EnumFlowerChromosome.SPECIES.ordinal()]
					.getSecondaryAllele();

			genome0 = genome;
			genome1 = mate;
		}

		Collections.shuffle(FlowerManager.flowerMutations);
		for (IFlowerMutation mutation : FlowerManager.flowerMutations) {
			int chance = 0;

			// Stop blacklisted species.
			// if(FlowerManager.breedingManager.isBlacklisted(mutation.getTemplate()[0].getUID()))
			// continue;

			if ((chance = mutation
					.getChance(allele0, allele1, genome0, genome1)) > 0)
				if (world.rand.nextInt(100) < chance) {
					IBotanistTracker breedingTracker = FlowerManager.breedingManager
							.getBotanistTracker(world);
					breedingTracker.registerMutation(mutation);
					return FlowerManager.flowerInterface
							.templateAsChromosomes(mutation.getTemplate());
				}
		}

		return null;
	}

	private IChromosome inheritChromosome(Random rand, IChromosome parent1,
			IChromosome parent2) {

		IAllele choice1;
		if (rand.nextBoolean())
			choice1 = parent1.getPrimaryAllele();
		else
			choice1 = parent1.getSecondaryAllele();

		IAllele choice2;
		if (rand.nextBoolean())
			choice2 = parent2.getPrimaryAllele();
		else
			choice2 = parent2.getSecondaryAllele();

		if (rand.nextBoolean())
			return new Chromosome(choice1, choice2);
		else
			return new Chromosome(choice2, choice1);
	}

	@Override
	public void mate(IFlowerGenome genome) {
		this.mate = genome;
	}

	@Override
	public boolean isPollinated() {
		return mate != null;
	}

	@Override
	public IFlower die(World world) {

		if (mate == null && world.rand.nextInt(3) > 0)
			mate = genome;

		return createOffspring(world);

	}

	@Override
	public void setAge(int age) {
		this.age = age;
	}

	@Override
	public IIndividual copy() {
		return null;
	}

}
