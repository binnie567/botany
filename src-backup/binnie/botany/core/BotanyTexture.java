package binnie.botany.core;

import binnie.core.IBinnieTexture;

public enum BotanyTexture implements IBinnieTexture {

	Main("/gfx/botany/botany.png"), Main2("/gfx/botany/botany2.png"), ;

	String texture;

	BotanyTexture(String texture) {
		this.texture = texture;
	}

	public String getTexture() {
		return texture;
	}
	
	public final static String guiPath = "/gfx/botany/GUI";

}
