package binnie.botany.core;

import java.util.Collection;

import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import binnie.botany.Botany;
import binnie.botany.api.IFlower;
import binnie.botany.genetics.Flower;
import binnie.botany.network.PacketFlowerAge;
import binnie.botany.network.PacketNewFlower;
import binnie.core.BinnieCore;
import forestry.api.farming.ICrop;
import forestry.plugins.PluginBotany;

public class TileEntityPlant extends TileEntity implements ICrop {

	public TileEntityPlant() {
		super();
		flower = new Flower(
				BotanyCore.flowerInterface.getDefaultFlowerGenome(), 0);
	}

	@Override
	public void updateEntity() {

		if (!BinnieCore.proxy.isSimulating(worldObj))
			return;

		if (newFlowerPacket) {
			newFlowerPacket = false;
			System.out.println("Sending Packet of "
					+ this.flower.getDisplayName() + " from Server");
			Botany.proxy.sendNetworkPacket(new PacketNewFlower(this), xCoord,
					yCoord, zCoord);
		}

		if (worldObj.getBlockId(xCoord, yCoord, zCoord) != PluginBotany.blockBotany.blockID)
			die();

		if (worldObj.getBlockId(xCoord, yCoord - 1, zCoord) != PluginBotany.blockLoam.blockID)
			return;

		if (worldObj.rand.nextInt(1) == 0)
			pollinate();

		if (worldObj.rand.nextInt(1) == 0) {
			if (flower.isPollinated()) {
				spawnOffspring();
			}
		}

		if (worldObj.rand.nextInt(50) == 0) {
			flower.age();
			Botany.proxy.sendNetworkPacket(new PacketFlowerAge(xCoord, yCoord,
					zCoord, flower.getAge()), xCoord, yCoord, zCoord);
			if (!flower.isAlive()) {
				IFlower offspring = flower.die(worldObj);
				if (offspring == null)
					die();
				else {
					this.flower = offspring;
					newFlowerPacket = true;
				}

			}
		}

	}

	public void pollinate() {

		int x = xCoord - worldObj.rand.nextInt(3) + worldObj.rand.nextInt(6);
		int y = yCoord - worldObj.rand.nextInt(3) + worldObj.rand.nextInt(6);
		int z = zCoord - worldObj.rand.nextInt(3) + worldObj.rand.nextInt(6);

		TileEntity entity = worldObj.getBlockTileEntity(x, y, z);

		if (entity == null)
			return;
		if (!(entity instanceof TileEntityPlant))
			return;

		IFlower target = ((TileEntityPlant) entity).flower;

		target.mate(flower.getFlowerGenome());

	}

	public void spawnOffspring() {
		int x = xCoord - worldObj.rand.nextInt(2) + worldObj.rand.nextInt(3);
		int y = yCoord - worldObj.rand.nextInt(2) + worldObj.rand.nextInt(3);
		int z = zCoord - worldObj.rand.nextInt(2) + worldObj.rand.nextInt(3);

		if (worldObj.getBlockId(x, y - 1, z) != PluginBotany.blockLoam.blockID)
			return;

		if (!worldObj.isAirBlock(x, y, z))
			return;

		IFlower offspring = flower.createOffspring(worldObj);

		if (offspring == null) {
			return;

		}

		worldObj.setBlockAndMetadataWithNotify(x, y, z,
				PluginBotany.blockBotany.blockID, 0);

		if (worldObj.getBlockId(x, y, z) == PluginBotany.blockBotany.blockID) {
			TileEntity tile = worldObj.getBlockTileEntity(x, y, z);
			if (tile instanceof TileEntityPlant) {
				((TileEntityPlant) tile).setFlower(offspring);
				Botany.proxy.sendNetworkPacket(new PacketNewFlower(
						(TileEntityPlant) tile), x, y, z);
			}

		}
	}

	public void die() {
		this.invalidate();
		worldObj.removeBlockTileEntity(xCoord, yCoord, zCoord);
		worldObj.setBlockAndMetadataWithNotify(xCoord, yCoord, zCoord, 0, 0);
	}

	@Override
	public boolean canUpdate() {
		return true;
	}

	IFlower flower;

	public void setFlower(IFlower flower) {
		this.flower = flower;
	}

	public int getDamage() {
		return flower.getDamage();
	}

	@Override
	public void writeToNBT(NBTTagCompound nbt) {
		super.writeToNBT(nbt);
		NBTTagCompound flowerNBT = new NBTTagCompound();
		flower.writeToNBT(flowerNBT);
		nbt.setCompoundTag("flower", flowerNBT);
	}

	@Override
	public void readFromNBT(NBTTagCompound nbt) {
		super.readFromNBT(nbt);
		if (nbt.hasKey("flower")) {
			flower = new Flower(nbt.getCompoundTag("flower"));
		} else {
			flower = new Flower(
					BotanyCore.flowerInterface.getDefaultFlowerGenome(), 0);
		}
		System.out.println("Loaded " + flower.getDisplayName() + "from NBT");
		newFlowerPacket = true;
	}

	boolean newFlowerPacket = false;

	public IFlower getFlower() {
		return flower;
	}

	public void setAge(int age) {
		flower.setAge(age);
	}

	@Override
	public Collection<ItemStack> harvest() {
		return null;
	}

}
