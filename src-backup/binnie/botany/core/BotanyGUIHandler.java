package binnie.botany.core;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;
import binnie.core.gui.BinnieGUIHandler;
import binnie.craftgui.botany.WindowFlowerDatabase;
import binnie.craftgui.window.Window;
import cpw.mods.fml.relauncher.Side;

public class BotanyGUIHandler extends BinnieGUIHandler {

	public Window getWindow(int id, EntityPlayer player, World world, int x,
			int y, int z, Side side) {

		if (id < 0 || id >= BotanyGUI.values().length)
			return null;

		BotanyGUI ID = BotanyGUI.values()[id];

		Window window = null;

		TileEntity tileEntity = world.getBlockTileEntity(x, y, z);

		IInventory object = null;

		if (tileEntity instanceof IInventory) {
			object = (IInventory) tileEntity;
		}

		switch (ID) {
		case Database:
			window = WindowFlowerDatabase.create(player, side,
					ID == BotanyGUI.Database ? false : true);
			break;
		}

		return window;

	}

}
