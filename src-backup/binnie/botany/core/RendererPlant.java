package binnie.botany.core;

import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.tileentity.TileEntity;
import binnie.botany.api.IFlower;
import binnie.core.BinnieCore;

public class RendererPlant extends TileEntitySpecialRenderer {

	public RendererPlant() {
	}

	public static void drawFace(Tessellator tessellator, double x1, double y1,
			double z1, double x2, double y2, double z2, double textLeft,
			double textRight, double textTop, double textBottom) {

		tessellator.addVertexWithUV(x1, y1, z1, textLeft, textTop);
		tessellator.addVertexWithUV(x1, y2, z1, textLeft, textBottom);
		tessellator.addVertexWithUV(x2, y2, z2, textRight, textBottom);
		tessellator.addVertexWithUV(x2, y1, z2, textRight, textTop);

	}

	@Override
	public void renderTileEntityAt(TileEntity entity, double xCoord,
			double yCoord, double zCoord, float par8) {
		if (!(entity instanceof TileEntityPlant))
			return;

		BinnieCore.proxy.bindTexture(BotanyTexture.Main.getTexture());

		TileEntityPlant plant = (TileEntityPlant) entity;

		IFlower flower = plant.getFlower();

		render3DFlower(flower, xCoord, yCoord, zCoord, par8);
	}

	public static void render3DFlower(IFlower flower, double xCoord,
			double yCoord, double zCoord, float par8) {

		int age = flower.getAge();

		Tessellator tessellator = Tessellator.instance;

		int iconIndex = age + flower.getFlowerGenome().getStem().getIconIndex()
				* 8;

		int var11 = (iconIndex & 15) << 4;
		int var12 = iconIndex & 240;

		double textLeft = var11 / 256.0F;
		double textRight = (var11 + 15.99F) / 256.0F;
		double textTop = var12 / 256.0F;
		double textBottom = (var12 + 15.99F) / 256.0F;
		double x1 = xCoord + 0.5D - 0.45D;
		double x2 = xCoord + 0.5D + 0.45D;
		double z1 = zCoord + 0.5D - 0.45D;
		double z2 = zCoord + 0.5D + 0.45D;
		tessellator.startDrawingQuads();

		tessellator.setColorOpaque(255, 255, 255);

		drawFace(tessellator, x1, yCoord + 1D, z1, x2, yCoord, z2, textLeft,
				textRight, textTop, textBottom);
		drawFace(tessellator, x2, yCoord + 1D, z2, x1, yCoord, z1, textLeft,
				textRight, textTop, textBottom);
		drawFace(tessellator, x1, yCoord + 1D, z2, x2, yCoord, z1, textLeft,
				textRight, textTop, textBottom);
		drawFace(tessellator, x2, yCoord + 1D, z1, x1, yCoord, z2, textLeft,
				textRight, textTop, textBottom);

		tessellator.addVertexWithUV(x1, yCoord + 1.0D, z2, textRight, textTop);

		tessellator.draw();

		iconIndex = age
				+ flower.getFlowerGenome().getPrimaryAsFlower()
						.getPrimaryIconIndex() * 8;

		var11 = (iconIndex & 15) << 4;
		var12 = iconIndex & 240;
		textLeft = var11 / 256.0F;
		textRight = (var11 + 15.99F) / 256.0F;
		textTop = var12 / 256.0F;
		textBottom = (var12 + 15.99F) / 256.0F;
		x1 = xCoord + 0.5D - 0.45D;
		x2 = xCoord + 0.5D + 0.45D;
		z1 = zCoord + 0.5D - 0.45D;
		z2 = zCoord + 0.5D + 0.45D;

		int colour = flower.getFlowerGenome().getPrimaryAsFlower()
				.getPrimaryColor();

		int r = colour / (256 * 256);
		colour = colour % (256 * 256);
		int g = colour / 256;
		colour = colour % 256;
		int b = colour;

		tessellator.startDrawingQuads();

		tessellator.setColorOpaque(r, g, b);

		drawFace(tessellator, x1, yCoord + 1D, z1, x2, yCoord, z2, textLeft,
				textRight, textTop, textBottom);
		drawFace(tessellator, x2, yCoord + 1D, z2, x1, yCoord, z1, textLeft,
				textRight, textTop, textBottom);
		drawFace(tessellator, x1, yCoord + 1D, z2, x2, yCoord, z1, textLeft,
				textRight, textTop, textBottom);
		drawFace(tessellator, x2, yCoord + 1D, z1, x1, yCoord, z2, textLeft,
				textRight, textTop, textBottom);

		tessellator.draw();

		iconIndex = age
				+ flower.getFlowerGenome().getPrimaryAsFlower()
						.getSecondaryIconIndex() * 8;

		var11 = (iconIndex & 15) << 4;
		var12 = iconIndex & 240;
		textLeft = var11 / 256.0F;
		textRight = (var11 + 15.99F) / 256.0F;
		textTop = var12 / 256.0F;
		textBottom = (var12 + 15.99F) / 256.0F;
		x1 = xCoord + 0.5D - 0.45D;
		x2 = xCoord + 0.5D + 0.45D;
		z1 = zCoord + 0.5D - 0.45D;
		z2 = zCoord + 0.5D + 0.45D;

		colour = flower.getFlowerGenome().getPrimaryAsFlower()
				.getSecondaryColor();

		r = colour / (256 * 256);
		colour = colour % (256 * 256);
		g = colour / 256;
		colour = colour % 256;
		b = colour;

		tessellator.startDrawingQuads();

		tessellator.setColorOpaque(r, g, b);

		drawFace(tessellator, x1, yCoord + 1D, z1, x2, yCoord, z2, textLeft,
				textRight, textTop, textBottom);
		drawFace(tessellator, x2, yCoord + 1D, z2, x1, yCoord, z1, textLeft,
				textRight, textTop, textBottom);
		drawFace(tessellator, x1, yCoord + 1D, z2, x2, yCoord, z1, textLeft,
				textRight, textTop, textBottom);
		drawFace(tessellator, x2, yCoord + 1D, z1, x1, yCoord, z2, textLeft,
				textRight, textTop, textBottom);

		tessellator.draw();

	}

}
