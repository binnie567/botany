package binnie.botany.core;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class ItemLoam extends ItemBlock {

	public ItemLoam(int i) {
		super(i);
		setCreativeTab(CreativeTabs.tabBlock);
	}

	@Override
	@SideOnly(Side.CLIENT)
	public String getItemDisplayName(ItemStack par1ItemStack) {
		return BlockLoam.getName(par1ItemStack.getItemDamage());
	}

	@Override
	public int getMetadata(int i) {
		return i;
	}

}
