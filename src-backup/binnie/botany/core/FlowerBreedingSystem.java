package binnie.botany.core;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import binnie.botany.api.FlowerManager;
import binnie.botany.api.IAlleleFlowerSpecies;
import binnie.botany.api.IFlower;
import binnie.botany.api.IFlowerGenome;
import binnie.core.genetics.BreedingSystem;
import forestry.api.genetics.IAllele;
import forestry.api.genetics.IAlleleSpecies;
import forestry.api.genetics.IMutation;

public class FlowerBreedingSystem extends
		BreedingSystem<IAlleleFlowerSpecies, IFlower> {

	public FlowerBreedingSystem() {
		super(IAlleleFlowerSpecies.class, IFlower.class);
	}

	@Override
	public IFlower speciesAsIndividual(IAlleleFlowerSpecies species) {
		if (species == null)
			return null;
		IAllele[] template = FlowerManager.breedingManager
				.getFlowerTemplate(species.getUID());
		if (template == null)
			return null;
		IFlowerGenome genome = FlowerManager.flowerInterface
				.templateAsGenome(template);
		IFlower bee = FlowerManager.flowerInterface.getFlower(genome, 4);
		return bee;
	}

	@Override
	public ItemStack getIndividualItemStack(IFlower individual) {
		ItemStack item = FlowerManager.flowerInterface
				.getFlowerStack(individual);
		return item;
	}

	@Override
	public IMutation[] getMutationArray() {
		return new IMutation[0];
	}
	@Override
	public int getChance(IMutation mutation, EntityPlayer player,
			IAllele species1, IAllele species2) {
		return 0;
	}

	@Override
	public String getDescriptor() {
		return "Botanist";
	}

}
