package binnie.botany.core;

import net.minecraft.block.Block;
import net.minecraft.block.BlockFlower;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;

public class BlockBotany extends BlockFlower {

	@Override
	public int getRenderType() {
		return -1;
	}

	@Override
	public boolean renderAsNormalBlock() {
		return false;
	}

	@Override
	public boolean isOpaqueCube() {
		return false;
	}

	@Override
	public boolean hasTileEntity(int metadata) {
		return true;
	}

	@Override
	public TileEntity createTileEntity(World world, int metadata) {
		return new TileEntityPlant();
	}

	public BlockBotany(int id) {
		super(id, 0);
		setCreativeTab(CreativeTabs.tabDecorations);
	}

	@Override
	protected boolean canThisPlantGrowOnThisBlockID(int par1) {
		return Block.blocksList[par1] != null
				&& Block.blocksList[par1].isOpaqueCube();
	}

}
