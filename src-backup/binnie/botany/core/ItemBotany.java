package binnie.botany.core;

import java.util.List;

import net.minecraft.block.Block;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;
import binnie.botany.api.IAlleleFlowerSpecies;
import binnie.botany.api.IAlleleFlowerStem;
import binnie.botany.api.IFlower;
import binnie.botany.genetics.Flower;
import binnie.botany.genetics.FlowerSpecies;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import forestry.api.genetics.IAllele;
import forestry.plugins.PluginBotany;

public class ItemBotany extends Item {

	@Override
	@SideOnly(Side.CLIENT)
	public void getSubItems(int par1, CreativeTabs par2CreativeTabs,
			List itemList) {
		super.getSubItems(par1, par2CreativeTabs, itemList);
		for (IAllele[] template : FlowerSpecies.getTemplates()) {
			IFlower individual = new Flower(
					BotanyCore.flowerInterface.templateAsGenome(template), 3);
			ItemStack someStack = BotanyCore.flowerInterface
					.getFlowerStack(individual);
			itemList.add(someStack);

			individual = new Flower(
					BotanyCore.flowerInterface.templateAsGenome(template), 0);
			someStack = BotanyCore.flowerInterface.getFlowerStack(individual);
			itemList.add(someStack);
		}
	}

	@Override
	@SideOnly(Side.CLIENT)
	public String getItemDisplayName(ItemStack par1ItemStack) {
		return getItemNameIS(par1ItemStack);
	}

	@Override
	@SideOnly(Side.CLIENT)
	public boolean requiresMultipleRenderPasses() {
		return true;
	}

	@Override
	public String getItemName() {
		return "??? Flower";
	}

	@Override
	public String getItemNameIS(ItemStack itemStack) {
		IFlower flower = BotanyCore.flowerInterface.getFlower(itemStack);
		return flower.getDisplayName();
	}

	@Override
	@SideOnly(Side.CLIENT)
	public int getColorFromItemStack(ItemStack itemstack, int pass) {
		int damage = itemstack.getItemDamage();
		if (pass > 0) {
			IAlleleFlowerSpecies species = BotanyCore.speciesFromDamage(damage);
			if (species != null)
				return species.getPrimaryColor();
		}
		return super.getColorFromItemStack(itemstack, pass);
	}

	@Override
	@SideOnly(Side.CLIENT)
	public int getIconFromDamageForRenderPass(int damage, int pass) {

		int age = BotanyCore.ageFromDamage(damage);

		if (pass == 0) {
			IAlleleFlowerStem stem = BotanyCore.stemFromDamage(damage);
			if (stem != null)
				return 16 + age;
		} else {
			IAlleleFlowerSpecies species = BotanyCore.speciesFromDamage(damage);
			if (species != null)
				return 24 + age;
		}

		return super.getIconFromDamageForRenderPass(damage, pass);
	}

	public ItemBotany(int i) {
		super(i);
		this.setCreativeTab(CreativeTabs.tabDecorations);
		setTextureFile("/gfx/botany/botany.png");
		this.blockID = PluginBotany.blockBotany.blockID;
	}

	int blockID = 0;

	@Override
	public boolean onItemUse(ItemStack par1ItemStack,
			EntityPlayer par2EntityPlayer, World par3World, int par4, int par5,
			int par6, int par7, float par8, float par9, float par10) {
		int var11 = par3World.getBlockId(par4, par5, par6);

		if (var11 == Block.snow.blockID) {
			par7 = 1;
		} else if (var11 != Block.vine.blockID
				&& var11 != Block.tallGrass.blockID
				&& var11 != Block.deadBush.blockID
				&& (Block.blocksList[var11] == null || !Block.blocksList[var11]
						.isBlockReplaceable(par3World, par4, par5, par6))) {
			if (par7 == 0) {
				--par5;
			}

			if (par7 == 1) {
				++par5;
			}

			if (par7 == 2) {
				--par6;
			}

			if (par7 == 3) {
				++par6;
			}

			if (par7 == 4) {
				--par4;
			}

			if (par7 == 5) {
				++par4;
			}
		}

		if (par1ItemStack.stackSize == 0) {
			return false;
		}
		// else if (!par2EntityPlayer.canPlayerEdit(par4, par5, par6))
		// {
		// return false;
		// }
		else if (par5 == 255
				&& Block.blocksList[this.blockID].blockMaterial.isSolid()) {
			return false;
		} else if (par3World.canPlaceEntityOnSide(this.blockID, par4, par5,
				par6, false, par7, par2EntityPlayer)) {
			Block var12 = Block.blocksList[this.blockID];

			if (placeBlockAt(par1ItemStack, par2EntityPlayer, par3World, par4,
					par5, par6, par7, par8, par9, par10)) {
				par3World.playSoundEffect(par4 + 0.5F, par5 + 0.5F,
						par6 + 0.5F, var12.stepSound.getStepSound(),
						(var12.stepSound.getVolume() + 1.0F) / 2.0F,
						var12.stepSound.getPitch() * 0.8F);
				--par1ItemStack.stackSize;
			}

			return true;
		} else {
			return false;
		}
	}

	public boolean placeBlockAt(ItemStack stack, EntityPlayer player,
			World world, int x, int y, int z, int side, float hitX, float hitY,
			float hitZ) {
		if (!world.setBlockAndMetadataWithNotify(x, y, z, this.blockID, 0)) {
			return false;
		}

		if (world.getBlockId(x, y, z) == this.blockID) {
			Block.blocksList[this.blockID].onBlockPlacedBy(world, x, y, z,
					player);
			TileEntity tile = world.getBlockTileEntity(x, y, z);
			if (tile instanceof TileEntityPlant)
				((TileEntityPlant) tile).setFlower(new Flower(stack
						.getTagCompound()));
		}

		return true;
	}

	public static boolean plantFlower(ItemStack germling, World world, int x,
			int y, int z) {
		return ((ItemBotany)PluginBotany.itemBotany).
				placeBlockAt(germling, null, world, x, y, z, 0, 0, 0, 0);
	}

}
