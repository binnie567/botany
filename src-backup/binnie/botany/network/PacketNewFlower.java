package binnie.botany.network;

import binnie.botany.core.TileEntityPlant;
import binnie.core.network.PacketTileNBT;

public class PacketNewFlower extends PacketTileNBT {

	public PacketNewFlower(TileEntityPlant tile) {
		super(PacketID.NewFlower.ordinal(), tile);
	}

	public PacketNewFlower() {
		super();
	}

}
