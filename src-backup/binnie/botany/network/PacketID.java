package binnie.botany.network;

import java.io.DataInputStream;
import java.io.IOException;

import net.minecraft.network.INetworkManager;
import net.minecraft.network.packet.Packet250CustomPayload;
import net.minecraft.tileentity.TileEntity;
import binnie.botany.core.TileEntityPlant;
import binnie.core.BinnieCore;
import cpw.mods.fml.common.network.Player;

public enum PacketID {

	FlowerAge, NewFlower

	;

	public void onPacketData(INetworkManager network,
			Packet250CustomPayload packet250, Player player,
			DataInputStream data) {
		if (this == FlowerAge) {
			try {
				PacketFlowerAge packet = new PacketFlowerAge();
				packet.readData(data);

				TileEntity tile = packet.getTileEntity(BinnieCore.proxy
						.getWorld());
				if (tile instanceof TileEntityPlant)
					((TileEntityPlant) tile).setAge(packet.age);
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else if (this == NewFlower) {
			try {
				PacketNewFlower packet = new PacketNewFlower();
				packet.readData(data);
				TileEntity tile = packet.getTarget(BinnieCore.proxy.getWorld());
				System.out.println("Recieved flower on Client");
				if (tile instanceof TileEntityPlant)
					((TileEntityPlant) tile).readFromNBT(packet
							.getTagCompound());
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

}
