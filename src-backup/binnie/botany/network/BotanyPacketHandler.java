package binnie.botany.network;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;

import net.minecraft.network.INetworkManager;
import net.minecraft.network.packet.Packet250CustomPayload;
import binnie.core.network.BinniePacketHandler;
import cpw.mods.fml.common.network.Player;

public class BotanyPacketHandler extends BinniePacketHandler {

	@Override
	public void onPacketData(INetworkManager manager,
			Packet250CustomPayload packet, Player player) {

		DataInputStream data = new DataInputStream(new ByteArrayInputStream(
				packet.data));

		try {
			int packetId = data.readByte();
			if (packetId < 0 || packetId >= PacketID.values().length)
				return;
			PacketID id = PacketID.values()[packetId];
			id.onPacketData(manager, packet, player, data);
		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}

}
