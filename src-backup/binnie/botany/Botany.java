package binnie.botany;

import binnie.botany.core.BotanyGUIHandler;
import binnie.botany.core.BotanyTexture;
import binnie.botany.network.BotanyPacketHandler;
import binnie.botany.proxy.Proxy;
import binnie.core.BinnieCore;
import binnie.core.IBinnieMod;
import binnie.core.IBinnieTexture;
import binnie.core.gui.BinnieGUIHandler;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.Instance;
import cpw.mods.fml.common.Mod.PreInit;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.network.NetworkMod;

@Mod(modid = "Botany", name = "Botany", version = "0.0")
@NetworkMod(clientSideRequired = true, serverSideRequired = true, channels = { "BOT" }, packetHandler = BotanyPacketHandler.class)
public class Botany implements IBinnieMod {

	@Instance("Botany")
	public static Botany instance;

	@SidedProxy(clientSide = "binnie.botany.proxy.ProxyClient", serverSide = "binnie.botany.proxy.ProxyServer")
	public static Proxy proxy;

	public static String channel = "BOT";

	public Botany() {
		BinnieCore.registerMod(this);
	};

	@PreInit
	public void preInit(FMLPreInitializationEvent evt) {
		Botany.instance = this;
	}

	@Override
	public BinnieGUIHandler getGUIHandler() {
		return new BotanyGUIHandler();
	}

	@Override
	public IBinnieTexture[] getTextures() {
		return BotanyTexture.values();
	}

	@Override
	public Class[] getConfigs() {
		return new Class[0];
	}

}
