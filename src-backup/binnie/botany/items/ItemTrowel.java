package binnie.botany.items;

import net.minecraft.block.Block;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.ForgeSubscribe;
import net.minecraftforge.event.entity.player.PlayerInteractEvent;
import net.minecraftforge.event.entity.player.PlayerInteractEvent.Action;
import binnie.botany.api.FlowerManager;
import binnie.botany.core.BotanyTexture;
import binnie.botany.core.TileEntityPlant;
import binnie.botany.genetics.FlowerSpecies;

public class ItemTrowel extends Item {

	@ForgeSubscribe
	public void onClick(PlayerInteractEvent event) {
		World world = event.entityPlayer.worldObj;
		int x = event.x;
		int y = event.y;
		int z = event.z;

		if (!(event.entityPlayer.getCurrentEquippedItem() != null && event.entityPlayer
				.getCurrentEquippedItem().getItem() instanceof ItemTrowel)) {
			return;
		}

		if (event.action != Action.LEFT_CLICK_BLOCK)
			return;

		if (world.getBlockId(x, y, z) == Block.plantRed.blockID) {
			if (!world.isRemote) {
				float var6 = 0.7F;
				double var7 = world.rand.nextFloat() * var6 + (1.0F - var6)
						* 0.5D;
				double var9 = world.rand.nextFloat() * var6 + (1.0F - var6)
						* 0.5D;
				double var11 = world.rand.nextFloat() * var6 + (1.0F - var6)
						* 0.5D;
				EntityItem var13 = new EntityItem(
						world,
						x + var7,
						y + var9,
						z + var11,
						FlowerManager.flowerInterface.getFlowerStack(FlowerManager.flowerInterface.getFlower(
								FlowerManager.flowerInterface
										.templateAsGenome(FlowerManager.breedingManager
												.getFlowerTemplate(FlowerSpecies.Rose
														.getUID())), 3)));
				var13.delayBeforeCanPickup = 10;
				world.spawnEntityInWorld(var13);
				world.setBlockWithNotify(x, y, z, 0);

				event.setCanceled(true);

			}
		}

		if (world.getBlockId(x, y, z) == Block.plantYellow.blockID) {
			if (!world.isRemote) {
				float var6 = 0.7F;
				double var7 = world.rand.nextFloat() * var6 + (1.0F - var6)
						* 0.5D;
				double var9 = world.rand.nextFloat() * var6 + (1.0F - var6)
						* 0.5D;
				double var11 = world.rand.nextFloat() * var6 + (1.0F - var6)
						* 0.5D;
				EntityItem var13 = new EntityItem(
						world,
						x + var7,
						y + var9,
						z + var11,
						FlowerManager.flowerInterface.getFlowerStack(FlowerManager.flowerInterface.getFlower(
								FlowerManager.flowerInterface
										.templateAsGenome(FlowerManager.breedingManager
												.getFlowerTemplate(FlowerSpecies.Dandelion
														.getUID())), 3)));
				var13.delayBeforeCanPickup = 10;
				world.spawnEntityInWorld(var13);
				world.setBlockWithNotify(x, y, z, 0);

				event.setCanceled(true);

			}
		}

		if (world.getBlockTileEntity(x, y, z) instanceof TileEntityPlant) {
			if (!world.isRemote) {
				float var6 = 0.7F;
				double var7 = world.rand.nextFloat() * var6 + (1.0F - var6)
						* 0.5D;
				double var9 = world.rand.nextFloat() * var6 + (1.0F - var6)
						* 0.5D;
				double var11 = world.rand.nextFloat() * var6 + (1.0F - var6)
						* 0.5D;
				EntityItem var13 = new EntityItem(world, x + var7, y + var9, z
						+ var11,
						FlowerManager.flowerInterface
								.getFlowerStack(((TileEntityPlant) world
										.getBlockTileEntity(x, y, z))
										.getFlower()));
				var13.delayBeforeCanPickup = 10;
				world.spawnEntityInWorld(var13);
				world.setBlockWithNotify(x, y, z, 0);
				world.setBlockTileEntity(x, y, z, null);
				event.setCanceled(true);

			}
		}
	}

	@Override
	public String getItemDisplayName(ItemStack par1ItemStack) {
		return "Botanist Trowel";
	}

	public ItemTrowel(int par1) {
		super(par1);
		setTextureFile(BotanyTexture.Main2.getTexture());
		this.setCreativeTab(CreativeTabs.tabTools);
		MinecraftForge.EVENT_BUS.register(this);
	}

}
