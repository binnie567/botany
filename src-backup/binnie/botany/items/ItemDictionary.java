package binnie.botany.items;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;
import binnie.botany.Botany;
import binnie.botany.core.BotanyGUI;
import binnie.botany.core.BotanyTexture;

public class ItemDictionary extends Item {

	public ItemDictionary(int par1) {
		super(par1);
		setIconIndex(1);
		setTextureFile(BotanyTexture.Main2.getTexture());
		setCreativeTab(CreativeTabs.tabTools);
	}

	@Override
	public ItemStack onItemRightClick(ItemStack itemstack, World world,
			EntityPlayer player) {
		Botany.proxy.openGui(BotanyGUI.Database, player, (int) player.posX,
				(int) player.posY, (int) player.posZ);
		return itemstack;
	}

	@Override
	public String getItemDisplayName(ItemStack i) {
		return "Botanist Database";
	}
}