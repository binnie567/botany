package binnie.botany.proxy;

import net.minecraft.entity.player.EntityPlayer;
import binnie.botany.core.BotanyGUI;
import binnie.core.network.BinniePacket;
import binnie.core.proxy.IProxyCore;

public interface IBotanyProxy extends IProxyCore {

	public void sendNetworkPacket(BinniePacket packet, int x, int y, int z);

	public void sendToPlayer(BinniePacket packet, EntityPlayer entityplayer);

	public void sendToServer(BinniePacket packet);

	public void openGui(BotanyGUI database, EntityPlayer player, int posX,
			int posY, int posZ);

}
