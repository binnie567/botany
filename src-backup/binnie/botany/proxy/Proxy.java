package binnie.botany.proxy;

import net.minecraft.entity.player.EntityPlayer;
import binnie.botany.Botany;
import binnie.botany.core.BotanyGUI;
import binnie.core.BinnieCore;
import binnie.core.network.BinniePacket;

public class Proxy implements IBotanyProxy {

	@Override
	public void sendNetworkPacket(BinniePacket packet, int x, int y, int z) {
		BinnieCore.proxy.sendNetworkPacket(packet, Botany.channel, x, y, z);
	}

	@Override
	public void sendToPlayer(BinniePacket packet, EntityPlayer entityplayer) {
		BinnieCore.proxy.sendToPlayer(packet, Botany.channel, entityplayer);
	}

	@Override
	public void sendToServer(BinniePacket packet) {
		BinnieCore.proxy.sendToServer(packet, Botany.channel);
	}

	@Override
	public void preInit() {
	}

	@Override
	public void doInit() {
	}

	@Override
	public void postInit() {
	}

	public void openGui(BotanyGUI id, EntityPlayer player, int x, int y, int z) {
		BinnieCore.proxy
				.openGui(Botany.instance, id.ordinal(), player, x, y, z);
	}

}
