package binnie.botany.api;

import forestry.api.genetics.IAlleleSpecies;

public interface IAlleleFlowerSpecies extends IAlleleSpecies {

	public String getTextureFile();

	public int getPrimaryIconIndex();

	public int getSecondaryIconIndex();
}
