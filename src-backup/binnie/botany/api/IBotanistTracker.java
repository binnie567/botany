package binnie.botany.api;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.nbt.NBTTagCompound;
import forestry.api.genetics.IAlleleSpecies;
import forestry.api.genetics.IIndividual;
import forestry.api.genetics.IMutation;

public interface IBotanistTracker {

	void decodeFromNBT(NBTTagCompound nbttagcompound);

	void encodeToNBT(NBTTagCompound nbttagcompound);

	void registerFlower(EntityPlayer player, IIndividual flower);

	int getFlowerCount();

	/**
	 * @return Integer denoting the amount of species discovered.
	 */
	int getSpeciesBred();

	void registerMutation(IMutation mutation);

	boolean isDiscovered(IMutation mutation);

	boolean isDiscovered(IAlleleSpecies species);

}
