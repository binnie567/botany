package binnie.botany.api;

import forestry.api.genetics.IGenome;

public interface IFlowerGenome extends IGenome {

	public IAlleleFlowerSpecies getPrimaryAsFlower();

	public IAlleleFlowerStem getStem();

}
