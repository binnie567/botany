package binnie.botany.api;

import net.minecraft.world.World;
import forestry.api.genetics.IAllele;

public interface IFlowerBreedingManager {

	int getFlowerSpeciesCount();

	void registerFlowerTemplate(IAllele[] template);

	void registerFlowerTemplate(String identifier, IAllele[] template);

	IAllele[] getFlowerTemplate(String identifier);

	IAllele[] getDefaultFlowerTemplate();

	IBotanistTracker getBotanistTracker(World world);

}
