package binnie.botany.api;

import net.minecraft.world.World;
import forestry.api.genetics.IIndividual;

public interface IFlower extends IIndividual {

	IFlowerGenome getFlowerGenome();

	public void age();

	public IFlower die(World world);

	public void mate(IFlowerGenome genome);

	int getAge();

	public boolean isAlive();

	public boolean isPollinated();

	public IFlower createOffspring(World world);

	int getDamage();

	void setAge(int age);

}
