package binnie.botany.api;

import java.util.ArrayList;

public class FlowerManager {

	public static IFlowerInterface flowerInterface;

	public static IFlowerBreedingManager breedingManager;

	/**
	 * List of possible mutations on species alleles.
	 */
	public static ArrayList<IFlowerMutation> flowerMutations = new ArrayList<IFlowerMutation>();

}
