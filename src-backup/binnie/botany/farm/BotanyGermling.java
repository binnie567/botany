package binnie.botany.farm;

import net.minecraft.item.ItemStack;
import net.minecraft.world.World;
import binnie.botany.core.ItemBotany;
import binnie.botany.core.TileEntityPlant;
import forestry.api.farming.ICrop;
import forestry.api.farming.IFarmable;
import forestry.plugins.PluginBotany;

public class BotanyGermling implements IFarmable {

	@Override
	public boolean isSaplingAt(World world, int x, int y, int z) {
		return world.getBlockTileEntity(x, y, z) instanceof TileEntityPlant;
	}

	@Override
	public ICrop getCropAt(World world, int x, int y, int z) {
		return (ICrop) world.getBlockTileEntity(x, y, z);
	}

	@Override
	public boolean isGermling(ItemStack itemstack) {
		return itemstack.getItem().itemID == PluginBotany.itemBotany.itemID;
	}

	@Override
	public boolean isWindfall(ItemStack itemstack) {
		return false;
	}

	@Override
	public boolean plantSaplingAt(ItemStack germling, World world, int x,
			int y, int z) {
		return ItemBotany.plantFlower(germling, world, x, y, z);
	}

}
