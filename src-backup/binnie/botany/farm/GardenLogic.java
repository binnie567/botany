package binnie.botany.farm;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Stack;

import net.minecraft.block.Block;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;
import net.minecraftforge.common.ForgeDirection;
import net.minecraftforge.liquids.LiquidStack;
import forestry.api.farming.ICrop;
import forestry.api.farming.IFarmHousing;
import forestry.api.farming.IFarmLogic;
import forestry.api.farming.IFarmable;
import forestry.core.utils.Utils;
import forestry.core.utils.Vect;
import forestry.plugins.PluginBotany;

public class GardenLogic implements IFarmLogic {

	private IFarmable botanyGermling;
	
	public GardenLogic(IFarmHousing housing) {
		this.housing = housing;
		botanyGermling = new BotanyGermling();
		//ground = new ItemStack[] {new ItemStack()}
	}

	@Override
	public boolean isAcceptedGermling(ItemStack itemstack) {
		return botanyGermling.isGermling(itemstack);
	}

	@Override
	public int getIconIndex() {
		return 12;
	}

	@Override
	public String getTextureFile() {
		return "/terrain.png";
	}

	@Override
	public String getName() {
		return "Garden";
	}
	
	
	
	
	
	protected boolean maintainCrops(int x, int y, int z, ForgeDirection direction, int extent) {
		
		for(int i = 0; i < extent; i++) {
			Vect position = translateWithOffset(x, y, z, direction, i);
			if(!isAirBlock(position)
					&& !Utils.isReplaceableBlock(world, position.x, position.y, position.z))
				continue;
			
			ItemStack below = getAsItemStack(position.add(new Vect(0, -1, 0)));
			if(PluginBotany.blockLoam.blockID != below.itemID)
				continue;
			
			return trySetCrop(position);
		}
		
		return false;
	}

	private boolean trySetCrop(Vect position) {
		return housing.plantGermling(botanyGermling, world, position.x, position.y, position.z);
	}
	
	@Override
	public Collection<ICrop> harvest(int x, int y, int z, ForgeDirection direction, int extent) {
		
		world = housing.getWorld();

		Stack<ICrop> crops = new Stack<ICrop>();
		for(int i = 0; i < extent; i++) {
			Vect position = translateWithOffset(x, y + 1, z, direction, i);
			ICrop crop = botanyGermling.getCropAt(world, position.x, position.y, position.z);
			if(crop != null)
				crops.push(crop);		
		}
		return crops;
	}
	
	
	
	

	ArrayList<ItemStack> produce = new ArrayList<ItemStack>();

	@Override
	public int getFertilizerConsumption() {
		return 20;
	}

	@Override
	public int getWaterConsumption(float hydrationModifier) {
		return (int)(20 * hydrationModifier);
	}
	
	@Override
	public boolean isAcceptedResource(ItemStack itemstack) {
		return itemstack.getItem().itemID == PluginBotany.blockLoam.blockID;
	}

	@Override
	public Collection<ItemStack> collect() {
		Collection<ItemStack> products = produce;
		produce = new ArrayList<ItemStack>();
		return products;
	}

	@Override
	public boolean cultivate(int x, int y, int z, ForgeDirection direction, int extent) {

		world = housing.getWorld();

		if(maintainSoil(x, y, z, direction, extent))
			return true;
		
		if(maintainWater(x, y, z, direction, extent))
			return true;
		
		if(maintainCrops(x, y + 1, z, direction, extent))
			return true;
		
		return false;
	}
	
	private boolean maintainWater(int x, int y, int z, ForgeDirection direction, int extent) {
		// Still not done, check water then
		for(int i = 0; i < extent; i++) {
			Vect position = translateWithOffset(x, y, z, direction, i);
			if(!isAirBlock(position)
					&& !Utils.isReplaceableBlock(world, position.x, position.y, position.z))
				continue;

			boolean isEnclosed = true;
			
			if(world.isAirBlock(position.x + 1, position.y, position.z))
				isEnclosed = false;
			else if(world.isAirBlock(position.x - 1, position.y, position.z))
				isEnclosed = false;
			else if(world.isAirBlock(position.x, position.y, position.z + 1))
				isEnclosed = false;
			else if(world.isAirBlock(position.x, position.y, position.z - 1))
				isEnclosed = false;
			
			if(isEnclosed) {
				return trySetWater(position);
			}
		}
		
		return false;
	}
	
	private boolean trySetWater(Vect position) {
		LiquidStack liquid = new LiquidStack(Block.waterStill.blockID, 1000);
		if(!housing.hasLiquid(liquid))
			return false;
		
		setBlock(position, Block.waterStill.blockID, 0);
		housing.removeLiquid(liquid);
		return true;
	}
	
	private boolean maintainSoil(int x, int y, int z, ForgeDirection direction, int extent) {
		
		for(int i = 0; i < extent; i++) {
			Vect position = translateWithOffset(x, y, z, direction, i);
			if(!isAirBlock(position) 
					&& !Utils.isReplaceableBlock(world, position.x, position.y, position.z)) {
				
				ItemStack block = getAsItemStack(position);
			/*	if(isWaste(block) && housing.hasResources(resource)) {
					produce.addAll(Block.blocksList[block.itemID].getBlockDropped(world, x, y, z, block.itemID, 0));
					setBlock(position, 0, 0);
					trySetSoil(position);
				}*/
				
				continue;
			}

			if(isManual)
				continue;
			
			if(i % 2 != 0) {
				ForgeDirection cclock = ForgeDirection.EAST;
				if(direction == ForgeDirection.EAST)
					cclock = ForgeDirection.SOUTH;
				else if(direction == ForgeDirection.SOUTH)
					cclock = ForgeDirection.EAST;
				else if(direction == ForgeDirection.WEST)
					cclock = ForgeDirection.SOUTH;					
				
				Vect previous = translateWithOffset(position.x, position.y, position.z, cclock, 1);
				ItemStack soil = getAsItemStack(previous);
				if(soil.getItem() == null || soil.getItem().itemID != PluginBotany.blockLoam.blockID)
					trySetSoil(position);
				continue;
			}
			
			return trySetSoil(position);
		}
		
		return false;
	}
	
	ItemStack[] getResources() {
		return new ItemStack[] {new ItemStack(PluginBotany.blockLoam, 1, 4)};
	}
	
	private boolean trySetSoil(Vect position) {
		ItemStack[] resources = getResources();
		if(!housing.hasResources(resources))
			return false;
		setBlock(position, PluginBotany.blockLoam.blockID, 4);
		housing.removeResources(getResources());
		return true;
	}
	
	
	
	
	
	
	
	
	World world;
	IFarmHousing housing;

	boolean isManual = false;
	
	protected final boolean isAirBlock(Vect position) {
		return world.isAirBlock(position.x, position.y, position.z);
	}
	
	protected final boolean isWoodBlock(Vect position) {
		int blockid = getBlockId(position);
		return Block.blocksList[blockid] != null
				&& Block.blocksList[blockid].isWood(world, position.x, position.y, position.z);
	}
	
	protected final int getBlockId(Vect position) {
		return world.getBlockId(position.x, position.y, position.z);
	}
	
	protected final int getBlockMeta(Vect position) {
		return world.getBlockMetadata(position.x, position.y, position.z);
	}
	
	protected final ItemStack getAsItemStack(Vect position) {
		return new ItemStack(getBlockId(position), 1, getBlockMeta(position));
	}
	
	protected final Vect translateWithOffset(int x, int y, int z, ForgeDirection direction, int step) {
		return new Vect(x + direction.offsetX*step, y + direction.offsetY*step, z + direction.offsetZ*step);
	}
	
	protected final void setBlock(Vect position, int id, int meta) {
		world.setBlockAndMetadataWithUpdate(position.x, position.y, position.z, id, meta, true);
	}	

}
