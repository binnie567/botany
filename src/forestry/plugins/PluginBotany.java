package forestry.plugins;

import net.minecraft.item.Item;
import net.minecraft.src.ModLoader;
import binnie.botany.api.IFlowerMutation;
import binnie.botany.core.BlockBotany;
import binnie.botany.core.BotanyCore;
import binnie.botany.core.RendererBotany;
import binnie.botany.core.TileEntityFlower;
import binnie.botany.genetics.AlleleFlowerStem;
import binnie.botany.genetics.FlowerMutation;
import binnie.botany.genetics.FlowerSpecies;
import binnie.core.BinnieCore;
import binnie.core.block.ItemMetadata;
import cpw.mods.fml.client.registry.RenderingRegistry;
import forestry.api.core.IPlugin;
import forestry.api.genetics.AlleleManager;
import forestry.api.genetics.IAllele;

//import binnie.botany.genetics.FlowerBranch;

public class PluginBotany implements IPlugin {
	
	public static Item itemBotany;
	static Item itemDictionary;
	public static BlockBotany blockBotany;

	@Override
	public boolean isAvailable() {
		return true;
	}

	@Override
	public void preInit() {
	}

	@Override
	public void doInit() {

		for (IAllele allele : AlleleFlowerStem.values()) {
			AlleleManager.alleleRegistry.registerAllele(allele);
		}

		for (FlowerSpecies species : FlowerSpecies.values()) {
			AlleleManager.alleleRegistry.registerAllele(species);
			BotanyCore.getFlowerRoot().registerTemplate(
					species.getUID(), species.getTemplate());
		}

		for (IFlowerMutation mutation : FlowerMutation.values()) {
			BotanyCore.getFlowerRoot().registerMutation(mutation);
		}

		// Block bb = new BlockBotany(2291);
		// ModLoader.registerBlock(bb);
		blockBotany = new BlockBotany(2290);
		//itemBotany = new ItemBotany(2292);
		//itemTrowel = new ItemTrowel(2293);

		ModLoader.registerBlock(blockBotany, ItemMetadata.class);
		BinnieCore.proxy.registerTileEntity(TileEntityFlower.class, "botany.tile.flower", null);

		RendererBotany.renderID = RenderingRegistry.getNextAvailableRenderId();
		BinnieCore.proxy.registerBlockRenderer(new RendererBotany());
		
		/*
		 * if (BinnieCore.proxy.isClient()) { List<ItemStack> loamBlocks = new
		 * ArrayList<ItemStack>(); blockLoam.getSubBlocks(0, null, loamBlocks);
		 * for (ItemStack loam : loamBlocks) ModLoader .addName(loam,
		 * BlockLoam.getName(loam.getItemDamage())); }
		 */

//		BinnieCore.proxy
//				.registerTileEntity(
//						TileEntityPlant.class,
//						"botany.plant",
//						BinnieCore.proxy
//								.createObject("binnie.botany.core.RendererPlant"));
//
//		BinnieCore.proxy.registerCustomItemRenderer(itemBotany.itemID,
//				new ItemRendererBotany());

	}

	Item itemTrowel;

	@Override
	public void postInit() {
		// FlowerBranch.init();
		for (IFlowerMutation mutation : FlowerMutation.values()) {
			BotanyCore.getFlowerRoot().registerMutation(mutation);
		}

//		ModLoader.addRecipe(
//				new ItemStack(itemTrowel),
//				new Object[] { " BR", " SB", "S  ", Character.valueOf('B'),
//						ItemInterface.getItem("ingotBronze"),
//						Character.valueOf('R'),
//						new ItemStack(Item.dyePowder, 1, 1),
//						Character.valueOf('S'), Item.stick });
		
//		CircuitGarden circuitGarden = new CircuitGarden();
//		
//		ChipsetManager.circuitRegistry.registerCircuit(circuitGarden);
//		
//		ICircuitLayout layoutManaged = ChipsetManager.circuitRegistry.getLayout("forestry.farms.managed");
//		
//		ChipsetManager.solderManager.addRecipe(layoutManaged, new ItemStack(Item.appleRed), circuitGarden);
//		
		
	}

}
