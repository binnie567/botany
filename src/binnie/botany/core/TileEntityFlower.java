package binnie.botany.core;

import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import binnie.botany.api.IFlower;
import binnie.core.block.TileEntityMetadata;
import forestry.api.apiculture.FlowerManager;

public class TileEntityFlower extends TileEntityMetadata {

	IFlower flower = null;
	
	@Override
	public void readFromNBT(NBTTagCompound nbttagcompound) {
		if(flower != null)
			flower.readFromNBT(nbttagcompound);
		super.readFromNBT(nbttagcompound);
	}

	@Override
	public void writeToNBT(NBTTagCompound nbttagcompound) {
		if(flower != null)
			flower.writeToNBT(nbttagcompound);
		super.writeToNBT(nbttagcompound);
	}

	@Override
	public int getTileMetadata() {
		return flower == null ? meta : flower.getMetadata();
	}

	public void create(ItemStack stack) {
		flower = BotanyCore.getFlowerRoot().getMember(stack);
	}
	
}
