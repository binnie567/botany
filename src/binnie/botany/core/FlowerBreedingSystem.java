package binnie.botany.core;

import net.minecraft.entity.player.EntityPlayer;
import binnie.botany.genetics.EnumFlowerChromosome;
import binnie.core.genetics.BreedingSystem;
import forestry.api.genetics.IAllele;
import forestry.api.genetics.IBreedingTracker;
import forestry.api.genetics.IMutation;
import forestry.api.genetics.ISpeciesRoot;

public class FlowerBreedingSystem extends BreedingSystem {

	@Override
	public float getChance(IMutation mutation, EntityPlayer player,
			IAllele species1, IAllele species2) {
		return 0;
	}

	@Override
	public String getDescriptor() {
		return "Botanist";
	}

	@Override
	public ISpeciesRoot getSpeciesRoot() {
		return BotanyCore.getFlowerRoot();
	}

	
	@Override
	public String getChromosomeName(int i) {
		EnumFlowerChromosome chromo = (EnumFlowerChromosome) getChromosome(i);
		switch(chromo) {
		case SPECIES:
			return "Species";
		case STALK:
			return "Stem";
		default:
			return "";
		}
	}

	@Override
	public int getColour() {
		return 0xF50097;
	}

	@Override
	public Class<? extends IBreedingTracker> getTrackerClass() {
		// TODO Auto-generated method stub
		return null;
	}

}
