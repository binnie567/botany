package binnie.botany.core;

import net.minecraft.block.Block;
import net.minecraft.client.renderer.RenderBlocks;
import net.minecraft.world.IBlockAccess;
import binnie.botany.api.IFlower;
import cpw.mods.fml.client.registry.ISimpleBlockRenderingHandler;

public class RendererBotany implements ISimpleBlockRenderingHandler {

	public static int renderID;
	public static IFlower currentFlower;

	@Override
	public void renderInventoryBlock(Block block, int metadata, int modelID,
			RenderBlocks renderer) {
		
	}

	@Override
	public boolean renderWorldBlock(IBlockAccess world, int x, int y, int z,
			Block block, int modelId, RenderBlocks renderer) {
		currentFlower = null;
		renderer.renderCrossedSquares(block, x, y, z);
		currentFlower = null;
		return false;
	}

	@Override
	public boolean shouldRender3DInInventory() {
		return false;
	}

	@Override
	public int getRenderId() {
		return renderID;
	}

}
