package binnie.botany.core;

import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.entity.EntityLiving;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.world.World;
import binnie.botany.Botany;
import binnie.core.block.BlockMetadata;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class BlockBotany extends BlockMetadata {

	@Override
	public int getRenderType() {
		return RendererBotany.renderID;
	}

	@Override
	@SideOnly(Side.CLIENT)
	public void registerIcons(IconRegister par1IconRegister) {
		blockIcon = Botany.proxy.registerBlockIcon("flower/stem.normal.2");

	}

	public BlockBotany(int par1) {
		super(par1, Material.plants);
	}
	
	@Override
	public TileEntity createNewTileEntity(World var1) {
		return new TileEntityFlower();
	}
	
	 /**
     * Returns a bounding box from the pool of bounding boxes (this means this box can change after the pool has been
     * cleared to be reused)
     */
    public AxisAlignedBB getCollisionBoundingBoxFromPool(World par1World, int par2, int par3, int par4)
    {
        return null;
    }

    /**
     * Is this block (a) opaque and (b) a full 1m cube?  This determines whether or not to render the shared face of two
     * adjacent blocks and also whether the player can attach torches, redstone wire, etc to this block.
     */
    public boolean isOpaqueCube()
    {
        return false;
    }

    /**
     * If this block doesn't render as an ordinary block it will return False (examples: signs, buttons, stairs, etc)
     */
    public boolean renderAsNormalBlock()
    {
        return false;
    }
    
    public void onBlockPlacedBy(World world, int x, int y, int z, EntityLiving living, ItemStack stack) {
    	super.onBlockPlacedBy(world, x, y, z, living, stack);
    	TileEntity flower = world.getBlockTileEntity(x, y, z);
    	if(flower != null && flower instanceof TileEntityFlower) {
    		((TileEntityFlower) flower).create(stack);
    	}
    }


}
