package binnie.botany.core;

import java.util.HashMap;
import java.util.Map;

import binnie.botany.api.IAlleleFlowerSpecies;
import binnie.botany.api.IAlleleFlowerStem;
import binnie.botany.api.IFlower;
import binnie.botany.api.IFlowerRoot;
import binnie.botany.genetics.AlleleFlowerStem;
import binnie.botany.genetics.FlowerHelper;
import binnie.botany.genetics.FlowerSpecies;
import binnie.core.genetics.BreedingSystem;

public class BotanyCore {

	Map<String, IAlleleFlowerSpecies> flowerSpecies = new HashMap<String, IAlleleFlowerSpecies>();

	static IFlowerRoot speciesRoot = new FlowerHelper();

	public static BreedingSystem flowerBreedingSystem = new FlowerBreedingSystem();

	public static IFlowerRoot getFlowerRoot() {
		return speciesRoot;
	}
	
	public static int ageFromDamage(int damage) {
		return damage % 16;
	}

	public static IAlleleFlowerSpecies speciesFromDamage(int damage) {

		int index = (damage % 1024) / 16;
		if ((index < 0) || (index >= FlowerSpecies.values().length))
			return null;
		return FlowerSpecies.values()[index];
	}

	public static IAlleleFlowerStem stemFromDamage(int damage) {
		int index = damage / 1024;
		if ((index < 0) || (index >= AlleleFlowerStem.values().length))
			return null;
		return AlleleFlowerStem.values()[index];
	}

	public static IFlower flowerFromDamage(int damage) {
		return null;
	}

	public static int damageFromFlower(IFlower flower) {
		return 0;
	}

}
