package binnie.botany;

import java.util.List;

import binnie.botany.proxy.Proxy;
import binnie.core.BinnieCoreMod;
import binnie.core.IBinnieTexture;
import binnie.core.gui.IBinnieGUID;
import binnie.core.plugin.IBinnieModule;
import binnie.core.proxy.IProxyCore;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.Instance;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.network.NetworkMod;

@Mod(modid = "Botany", name = "Botany", version = "@VERSION@", dependencies = "after:BinnieCore")
@NetworkMod(clientSideRequired = true, serverSideRequired = true)
public class Botany extends BinnieCoreMod {

	@Instance("Botany")
	public static Botany instance;

	@SidedProxy(clientSide = "binnie.botany.proxy.ProxyClient", serverSide = "binnie.botany.proxy.ProxyServer")
	public static Proxy proxy;

	public Botany() {
		super();
		this.instance = this;
	};

	@Override
	public IBinnieGUID[] getGUIDs() {
		return new IBinnieGUID[0];
	}

	@Override
	public IBinnieTexture[] getTextures() {
		return new IBinnieTexture[0];
	}

	@Override
	public Class[] getConfigs() {
		return new Class[0];
	}

	@Override
	public String getChannel() {
		return "BOT";
	}

	@Override
	public IProxyCore getProxy() {
		return proxy;
	}

	@Override
	public String getId() {
		return "botany";
	}

	@Override
	public void registerModules(List<IBinnieModule> list) {
		// TODO Auto-generated method stub
		
	}

}
