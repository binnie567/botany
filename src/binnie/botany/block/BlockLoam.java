package binnie.botany.block;

import java.util.List;

import net.minecraft.block.BlockDirt;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Icon;
import binnie.botany.Botany;
import binnie.botany.api.EnumAcidity;
import binnie.botany.api.EnumMoisture;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class BlockLoam extends BlockDirt {

	@Override
	@SideOnly(Side.CLIENT)
	public void getSubBlocks(int par1, CreativeTabs par2CreativeTabs, List par3List) {
		for(int i = 0; i < 16; i++) {
			par3List.add(new ItemStack(this, 1, i));
		}
	}

	Icon[] icons = new Icon[16];
	
	@Override
	@SideOnly(Side.CLIENT)
	public Icon getIcon(int side, int meta) {
		EnumMoisture moisture = EnumMoisture.values()[meta % 4];
		EnumAcidity acidity = EnumAcidity.values()[meta / 4];
		return icons[meta];
	}

	@Override
	@SideOnly(Side.CLIENT)
	public void registerIcons(IconRegister par1IconRegister) {
		for(int i = 0; i < 16; i++) {
			icons[i] = Botany.proxy.registerBlockIcon("loam"+(i/4)+""+(i%4));
		}
	}

	protected BlockLoam(int id) {
		super(id);
		setCreativeTab(CreativeTabs.tabDecorations);
	}

}
