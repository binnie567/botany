//package binnie.botany.farm;
//
//import java.util.List;
//
//import net.minecraft.tileentity.TileEntity;
//import net.minecraftforge.common.ForgeDirection;
//import forestry.api.circuits.ICircuit;
//import forestry.api.farming.IFarmHousing;
//
//public class CircuitGarden implements ICircuit {
//
//	@Override
//	public String getUID() {
//		return "botany.circuit.garden";
//	}
//
//	@Override
//	public boolean requiresDiscovery() {
//		return false;
//	}
//
//	@Override
//	public int getLimit() {
//		return 4;
//	}
//
//	@Override
//	public String getName() {
//		return "Garden";
//	}
//
//	@Override
//	public boolean isCircuitable(TileEntity tile) {
//		return tile instanceof IFarmHousing;
//	}
//
//	@Override
//	public void onInsertion(int slot, TileEntity tile) {
//		((IFarmHousing)tile).setFarmLogic(ForgeDirection.values()[slot + 2], new GardenLogic((IFarmHousing) tile));
//	}
//
//	@Override
//	public void onLoad(int slot, TileEntity tile) {
//		onInsertion(slot, tile);
//	}
//
//	@Override
//	public void onRemoval(int slot, TileEntity tile) {
//		if(!isCircuitable(tile))
//			return;
//		
//		((IFarmHousing)tile).resetFarmLogic(ForgeDirection.values()[slot + 2]);
//	}
//
//	@Override
//	public void onTick(int slot, TileEntity tile) {
//	}
//
//	@Override
//	public void addTooltip(List<String> list) {
//		list.add(" - Flowers");
//	}
//
//}
