package binnie.botany.api;

public enum EnumMoisture {
	
	Damp("Damp "),
	Moist("Moist "),
	Normal(""),
	Dry("Dry "),
	
	;
	
	String qualifier;

	private EnumMoisture(String qualifier) {
		this.qualifier = qualifier;
	}
	
	

}
