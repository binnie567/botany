package binnie.botany.api;

import java.util.Collection;

import net.minecraft.item.ItemStack;
import net.minecraft.world.World;
import forestry.api.genetics.IAllele;
import forestry.api.genetics.ISpeciesRoot;

public interface IFlowerRoot extends ISpeciesRoot {
	
	IFlower getMember(ItemStack stack);

	IFlower templateAsIndividual(IAllele[] template);
	
	IFlower templateAsIndividual(IAllele[] templateActive, IAllele[] templateInactive);
	
	IFlowerGenome templateAsGenome(IAllele[] template);

	IFlowerGenome templateAsGenome(IAllele[] templateActive, IAllele[] templateInactive);

	IBotanistTracker getBreedingTracker(World world, String player);
	
	Collection<IFlowerMutation> getMutations(boolean shuffle);

	EnumFlowerType getType(ItemStack stack);
	
	IFlower getFlower(World world, IFlowerGenome genome);
	
	
}
