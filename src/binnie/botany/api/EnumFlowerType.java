package binnie.botany.api;

public enum EnumFlowerType {
	
	NONE("None"), SEED("Seed"), POLLEN("Pollen"), FLOWER("Flower"), 
	;

	public static final EnumFlowerType[] VALUES = values();
	
	String name;

	private EnumFlowerType(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

}
