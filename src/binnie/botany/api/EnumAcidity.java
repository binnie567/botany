package binnie.botany.api;

public enum EnumAcidity {
	
	VeryAcidic("Very Acidic "),
	Acidic("Acidic "),
	Normal(""),
	Alkaline("Alkaline "),
	
	;
	
	String qualifier;

	private EnumAcidity(String qualifier) {
		this.qualifier = qualifier;
	}
	
	

}
