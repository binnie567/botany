package binnie.botany.api;

import net.minecraft.util.Icon;
import forestry.api.genetics.IAlleleSpecies;

public interface IAlleleFlowerSpecies extends IAlleleSpecies {

	public String getTextureFile();

	public Icon getPrimaryIcon();

	public Icon getSecondaryIcon();
	
	public int getPrimaryColor();
	
	public int getSecondaryColor();
}
