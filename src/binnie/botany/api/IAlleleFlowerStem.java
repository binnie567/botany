package binnie.botany.api;

import forestry.api.genetics.IAllele;

public interface IAlleleFlowerStem extends IAllele {

	public String getTextureFile();

	public int getIconIndex();

}
