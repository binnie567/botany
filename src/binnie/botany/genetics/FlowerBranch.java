//package binnie.botany.genetics;
//
//import java.util.ArrayList;
//import java.util.List;
//
//import forestry.api.genetics.AlleleManager;
//import forestry.api.genetics.IAlleleSpecies;
//import forestry.api.genetics.IBranch;
//import binnie.botany.api.IFlowerBranch;
//
//public enum FlowerBranch implements IClassification {
//
//	ROSE("Rose", "Rosaceae Rosa", ""),
//	DANDELION("Dandelion", "Assorted", ""),
//	MARIGOLD("Marigold", "Asteraceae Tagetes", ""),
//	CORNFLOWER("Cornflower", "Asteraceae Centaurea", ""),
//	PANSY("Pansy", "Violaceae Viola", ""),
//	IRIS("Iris", "Iridaceae Iris", "");
//	
//	public static void init() {
//		ROSE.addMember(FlowerSpecies.Rose);
//		ROSE.addMember(FlowerSpecies.DogRose);
//		ROSE.addMember(FlowerSpecies.FrenchRose);
//		ROSE.addMember(FlowerSpecies.WhiteRose);
//		ROSE.addMember(FlowerSpecies.AustrianBriar);
//		
//		DANDELION.addMember(FlowerSpecies.Dandelion);
//		DANDELION.addMember(FlowerSpecies.Chicory);
//		DANDELION.addMember(FlowerSpecies.Salsify);
//		DANDELION.addMember(FlowerSpecies.DesertDandelion);
//		
//		MARIGOLD.addMember(FlowerSpecies.AfricanMarigold);
//		MARIGOLD.addMember(FlowerSpecies.FrenchMarigold);
//		MARIGOLD.addMember(FlowerSpecies.CornMarigold);
//		MARIGOLD.addMember(FlowerSpecies.DesertMarigold);
//		
//		CORNFLOWER.addMember(FlowerSpecies.Cornflower);
//		CORNFLOWER.addMember(FlowerSpecies.PerennialCornflower);
//		CORNFLOWER.addMember(FlowerSpecies.AmericanStarthistle);
//		CORNFLOWER.addMember(FlowerSpecies.PersianCornflower);
//		
//		PANSY.addMember(FlowerSpecies.FieldPansy);
//		PANSY.addMember(FlowerSpecies.NativeViolet);
//		PANSY.addMember(FlowerSpecies.YellowPansy);
//		PANSY.addMember(FlowerSpecies.FenViolet);
//		PANSY.addMember(FlowerSpecies.BlueViolet);
//		
//		IRIS.addMember(FlowerSpecies.BloodIris);
//		IRIS.addMember(FlowerSpecies.MunzIris);
//		IRIS.addMember(FlowerSpecies.YellowIris);
//		IRIS.addMember(FlowerSpecies.EnglishIris);
//		
//		for(IBranch branch : values()) AlleleManager.alleleRegistry.registerBranch(branch);
//
//	}
//
//	private FlowerBranch(String name, String scientific, String description) {
//		this.name = name;
//		this.scientific = scientific;
//		this.description = description;
//	}
//
//	@Override
//	public String getUID() {
//		return "botany.flowers.branches." + this.toString().toLowerCase();
//	}
//	
//	String name;
//	String scientific;
//	String description;
//
//	@Override
//	public String getName() {
//		return name;
//	}
//
//	@Override
//	public String getScientific() {
//		return scientific;
//	}
//
//	@Override
//	public String getDescription() {
//		return description;
//	}
//	
//	List<IAlleleSpecies> members = new ArrayList<IAlleleSpecies>();
//
//	@Override
//	public IAlleleSpecies[] getMembers() {
//		return members.toArray(new IAlleleSpecies[0]);
//	}
//
//	@Override
//	public void addMember(IAlleleSpecies species) {
//		members.add(species);
//	}
//	
//
// }
