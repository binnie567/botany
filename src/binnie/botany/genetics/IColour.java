package binnie.botany.genetics;

public interface IColour {

	public static int BLACK = 0x191919;
	public static int RED = 0xCC4C4C;
	public static int GREEN = 0x667F33;
	public static int BROWN = 0x7F664C;

	public static int BLUE = 0x3366CC;
	public static int PURPLE = 0xB266E5;
	public static int CYAN = 0x4C99B2;
	public static int LGRAY = 0x999999;

	public static int GRAY = 0x4C4C4C;
	public static int PINK = 0xF2B2CC;
	public static int LGREEN = 0x7FCC19;
	public static int YELLOW = 0xE5E533;

	public static int LBLUE = 0x99B2F2;
	public static int MAGENTA = 0xE57FD8;
	public static int ORANGE = 0xF2B233;
	public static int WHITE = 0xFFFFFF;

}
