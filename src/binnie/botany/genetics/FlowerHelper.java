package binnie.botany.genetics;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;

import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.world.World;
import binnie.botany.api.EnumFlowerType;
import binnie.botany.api.IAlleleFlowerSpecies;
import binnie.botany.api.IBotanistTracker;
import binnie.botany.api.IFlower;
import binnie.botany.api.IFlowerGenome;
import binnie.botany.api.IFlowerMutation;
import binnie.botany.api.IFlowerRoot;
import binnie.core.genetics.BinnieGenetics;
import forestry.api.genetics.AlleleManager;
import forestry.api.genetics.IAllele;
import forestry.api.genetics.IChromosome;
import forestry.api.genetics.IChromosomeType;
import forestry.api.genetics.IIndividual;
import forestry.api.genetics.IMutation;
import forestry.plugins.PluginBotany;

public class FlowerHelper implements IFlowerRoot {
	
	public static int flowerSpeciesCount = -1;
	private static final String uid = "rootFlowers";
	
	@Override
	public String getUID() {
		return uid;
	}

	@Override
	public int getSpeciesCount() {
		if (flowerSpeciesCount < 0) {
			flowerSpeciesCount = 0;
			Iterator it = AlleleManager.alleleRegistry.getRegisteredAlleles().entrySet().iterator();
			while (it.hasNext()) {
				Entry<String, IAllele> entry = (Entry<String, IAllele>) it.next();
				if (entry.getValue() instanceof IAlleleFlowerSpecies)
					if (((IAlleleFlowerSpecies) entry.getValue()).isCounted())
						flowerSpeciesCount++;
			}
		}

		return flowerSpeciesCount;
	}

	@Override
	public boolean isMember(ItemStack stack) {
		return getType(stack) != EnumFlowerType.NONE;
	}

	@Override
	public boolean isMember(ItemStack stack, int type) {
		return getType(stack).ordinal() == type;
	}

	@Override
	public boolean isMember(IIndividual individual) {
		return individual instanceof IFlower;
	}

	@Override
	public ItemStack getMemberStack(IIndividual flower, int type) {
		if(!isMember(flower))
			return null;
		
		Item flowerItem = PluginBotany.itemBotany;

		NBTTagCompound nbttagcompound = new NBTTagCompound("tag");
		flower.writeToNBT(nbttagcompound);
		ItemStack flowerStack = new ItemStack(flowerItem);
		flowerStack.setTagCompound(nbttagcompound);
		return flowerStack;
	}


	@Override
	public EnumFlowerType getType(ItemStack stack) {
		if (stack == null)
			return EnumFlowerType.NONE;
		
		return stack.getItem() == PluginBotany.itemBotany ? EnumFlowerType.FLOWER : EnumFlowerType.NONE;
	}

	@Override
	public IFlower getMember(ItemStack stack) {
		if (!isMember(stack))
			return null;

		return new Flower(stack.getTagCompound());
	}

	@Override
	public IFlower getFlower(World world, IFlowerGenome genome) {
		return new Flower(genome, 1);
	}

	/* GENOME CONVERSIONS */
	@Override
	public IChromosome[] templateAsChromosomes(IAllele[] template) {
		return BinnieGenetics.getBeeRoot().templateAsChromosomes(template);
	}

	@Override
	public IChromosome[] templateAsChromosomes(IAllele[] templateActive, IAllele[] templateInactive) {
		return BinnieGenetics.getBeeRoot().templateAsChromosomes(templateActive, templateInactive);
	}

	@Override
	public IFlowerGenome templateAsGenome(IAllele[] template) {
		return new FlowerGenome(templateAsChromosomes(template));
	}

	@Override
	public IFlowerGenome templateAsGenome(IAllele[] templateActive, IAllele[] templateInactive) {
		return new FlowerGenome(templateAsChromosomes(templateActive, templateInactive));
	}

	@Override
	public IFlower templateAsIndividual(IAllele[] template) {
		return new Flower(templateAsGenome(template), 1);
	}

	@Override
	public IFlower templateAsIndividual(IAllele[] templateActive, IAllele[] templateInactive) {
		return new Flower(templateAsGenome(templateActive, templateInactive), 1);
	}

	/* TEMPLATES */
	public static HashMap<String, IAllele[]> speciesTemplates = new HashMap<String, IAllele[]>();
	public static ArrayList<IFlower> flowerTemplates = new ArrayList<IFlower>();

	@Override
	public Map<String, IAllele[]> getGenomeTemplates() {
		return speciesTemplates;
	}
	
	@Override
	public ArrayList<IFlower> getIndividualTemplates() {
		return flowerTemplates;
	}
	
	@Override
	public void registerTemplate(IAllele[] template) {
		registerTemplate(template[0].getUID(), template);
	}

	@Override
	public void registerTemplate(String identifier, IAllele[] template) {
		flowerTemplates.add(new Flower(templateAsGenome(template), 1));
		speciesTemplates.put(identifier, template);
	}

	@Override
	public IAllele[] getTemplate(String identifier) {
		return speciesTemplates.get(identifier);
	}

	@Override
	public IAllele[] getDefaultTemplate() {
		IAllele[] template = new IAllele[EnumFlowerChromosome.values().length];
		template[EnumFlowerChromosome.SPECIES.ordinal()] = FlowerSpecies.AfricanMarigold;
		return template;
	}

	@Override
	public IAllele[] getRandomTemplate(Random rand) {
		return speciesTemplates.values().toArray(new IAllele[0][])[rand.nextInt(speciesTemplates.values().size())];
	}

	/* MUTATIONS */
	/**
	 * List of possible mutations on species alleles.
	 */
	private static ArrayList<IFlowerMutation> flowerMutations = new ArrayList<IFlowerMutation>();

	@Override
	public Collection<IFlowerMutation> getMutations(boolean shuffle) {
		if (shuffle)
			Collections.shuffle(flowerMutations);
		return flowerMutations;
	}

	@Override
	public void registerMutation(IMutation mutation) {
		if (AlleleManager.alleleRegistry.isBlacklisted(mutation.getTemplate()[0].getUID()))
			return;
		if (AlleleManager.alleleRegistry.isBlacklisted(mutation.getAllele0().getUID()))
			return;
		if (AlleleManager.alleleRegistry.isBlacklisted(mutation.getAllele1().getUID()))
			return;

		flowerMutations.add((IFlowerMutation)mutation);
	}

	@Override
	public IBotanistTracker getBreedingTracker(World world, String player) {
//		String filename = "ApiaristTracker." + player;
//		BotanistTracker tracker = (BotanistTracker) world.loadItemData(BotanistTracker.class, filename);
//
//		// Create a tracker if there is none yet.
//		if (tracker == null) {
//			tracker = new BotanistTracker(filename);
//			world.setItemData(filename, tracker);
//		}

		return null;
	}

	@Override
	public IIndividual getMember(NBTTagCompound compound) {
		return new Flower(compound);
	}

	@Override
	public Class getMemberClass() {
		return null;
	}

	@Override
	public Collection<? extends IMutation> getCombinations(IAllele other) {
		return null;
	}

	@Override
	public Collection<? extends IMutation> getPaths(IAllele result,
			int chromosomeOrdinal) {
		return null;
	}

	@Override
	public Map<ItemStack, Float> getResearchCatalysts() {
		return new HashMap<ItemStack, Float>();
	}

	@Override
	public void setResearchSuitability(ItemStack itemstack, float suitability) {
	}

	@Override
	public IChromosomeType[] getKaryotype() {
		return EnumFlowerChromosome.values();
	}

}
