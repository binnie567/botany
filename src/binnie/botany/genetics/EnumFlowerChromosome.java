package binnie.botany.genetics;

import forestry.api.genetics.IAllele;
import forestry.api.genetics.IChromosomeType;
import forestry.api.genetics.ISpeciesRoot;
import forestry.plugins.PluginBotany;

public enum EnumFlowerChromosome implements IChromosomeType {
	SPECIES, STALK;

	@Override
	public Class<? extends IAllele> getAlleleClass() {
		return IAllele.class;
	}

	@Override
	public String getName() {
		return null;
	}

	@Override
	public ISpeciesRoot getSpeciesRoot() {
		return null;
	}
}
